/* eslint-env jest */
import { matchPathIdentityId } from 'queue-shared/auth-util'; // eslint-disable-line import/no-extraneous-dependencies
import AWS from 'aws-sdk';

jest.mock('aws-sdk');

describe('stacks lambda layer queue-shared auth-util', () => {
  it('calls', async () => {
    AWS.config.credentials.identityId = 22;
    await expect(matchPathIdentityId('token', 22)).resolves.not.toThrow();
  });

  it('throws when they dont match', async () => {
    AWS.config.credentials.identityId = 22;
    await expect(matchPathIdentityId('token', 44)).rejects.toThrow(
      'does not match',
    );
  });

  it('throws from aws errors', async () => {
    AWS.config.credentials.get.mockImplementation((cb) => {
      cb(new Error('foo'));
    });
    await expect(matchPathIdentityId('token', 44)).rejects.toThrow('foo');
  });
});

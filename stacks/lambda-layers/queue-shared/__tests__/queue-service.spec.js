/* eslint-env jest */
import AWS from 'aws-sdk';
import queueService from 'queue-shared/queue-service'; // eslint-disable-line import/no-extraneous-dependencies

jest.mock('aws-sdk');

describe('stacks lambda layer queue-shared queue-service', () => {
  describe('getQueue', () => {
    it('calls', async () => {
      AWS.DocClientGetFn.mockClear();
      await queueService.getQueue(22);
      expect(AWS.DocClientGetFn.mock.calls[0][0].ExpressionAttributeValues).toEqual({
        ':identityId': 22,
      });
    });

    it('returns empty array for no queue', async () => {
      AWS.DocClientGetFn.mockClear();
      AWS.SetDocClientGetFnResult({ Items: [] });
      const result = await queueService.getQueue(22);
      expect(result).toEqual([]);
    });
  });
});

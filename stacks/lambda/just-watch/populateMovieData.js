'use strict';

const AWS = require('aws-sdk');

function cleanJustWatchResponse(data) {
  return Object.entries(data).reduce((acc, x) => {
    const [key, val] = x;
    if (val !== '') {
      if (typeof val === 'object') {
        acc[key] = cleanJustWatchResponse(val);
      } else {
        acc[key] = val;
      }
    }
    return acc;
  }, {});
}

async function updateJustWatch({
  identityId,
  netflixId,
  justWatchResponse,
  justwatchId,
  objectType,
}) {
  const docClient = new AWS.DynamoDB.DocumentClient({
    apiVersion: '2012-08-10',
  });

  let updateExpression = 'SET updatedAt = :updatedAt, justwatchResponse = :nd, objectType = :objtype';
  const updateExpressionAttributeValues = {
    ':nd': justWatchResponse,
    ':updatedAt': new Date().toISOString(),
    ':objtype': objectType || 'movie',
  };
  if (justwatchId) {
    if (justwatchId === 'unset') {
      updateExpression += ' REMOVE justwatchId';
    } else {
      updateExpression += ', justwatchId = :jwid';
      updateExpressionAttributeValues[':jwid'] = justwatchId;
    }
  }

  return docClient
    .update({
      TableName: process.env.queueTable,
      Key: { identityId, netflixId },
      UpdateExpression: updateExpression,
      ExpressionAttributeValues: updateExpressionAttributeValues,
    })
    .promise();
}

async function populateMovieData({
  justWatchService,
  queueService,
  netflixId,
  identityId,
  justwatchId: requestJustwatchId,
  objectType: requestObjectType,
}) {
  let queue = [];
  try {
    queue = await queueService.getQueue(identityId);
  } catch (e) {
    throw new Error('User has no queue');
  }
  const origDocIndex = queue.findIndex((x) => x.netflixId === netflixId);
  if (origDocIndex === -1) {
    throw new Error(
      `The movie with netflixId ${netflixId} was not found in the current queue`,
    );
  }

  const {
    title,
    justwatchId: origDocJustwatchId,
    objectType: origDocObjType,
  } = queue[origDocIndex];
  const justwatchId = requestJustwatchId || origDocJustwatchId;
  const objectType = requestObjectType || origDocObjType;

  const justWatchResponse = cleanJustWatchResponse(
    await justWatchService.getJustWatchData(title, justwatchId, objectType),
  );

  await updateJustWatch({
    identityId,
    netflixId,
    justWatchResponse,
    justwatchId,
    objectType,
  });

  return justWatchResponse;
}

module.exports = {
  populateMovieData,
  updateJustWatch,
  cleanJustWatchResponse,
};

'use strict';

const queueService = require('queue-shared/queue-service'); // eslint-disable-line import/no-extraneous-dependencies
const { matchPathIdentityId } = require('queue-shared/auth-util'); // eslint-disable-line import/no-extraneous-dependencies
const { populateMovieData } = require('./populateMovieData');
const justWatchService = require('./just-watch-service');

exports.handler = async (event) => {
  const {
    netflixId,
    identityId,
    justwatchId,
    authorizationToken,
    objectType,
  } = event;

  const token = authorizationToken.split(' ')[1];

  try {
    await matchPathIdentityId(token, identityId);

    const response = await populateMovieData({
      queueService,
      justWatchService,
      netflixId,
      identityId,
      justwatchId,
      objectType,
    });

    return {
      statusCode: 200,
      headers: {},
      body: JSON.stringify(response),
      isBase64Encoded: false,
    };
  } catch (e) {
    e.message = `Error: ${e.message}`;
    throw e;
  }
};

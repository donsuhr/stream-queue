// request-promise-native in lambda-layers/queue-vender

'use strict';

const request = require('request-promise-native'); // eslint-disable-line import/no-extraneous-dependencies

async function searchByTitle(movieTitle) {
  const titleEncoded = encodeURIComponent(movieTitle.toLowerCase()).replace(
    /%20/g,
    '+',
  );

  const body = {
    age_certifications: null,
    content_types: null,
    genres: null,
    languages: null,
    max_price: null,
    min_price: null,
    presentation_types: null,
    providers: ['nfx', 'hlu', 'amp', 'amz', 'vdu', 'hbg', 'itu'],
    query: '~~~~',
    release_year_from: null,
    release_year_until: null,
    scoring_filter_types: null,
    timeline_type: null,
  };

  const bodyEncoded = encodeURI(JSON.stringify(body)).replace(
    '~~~~',
    titleEncoded,
  );
  const url = `https://apis.justwatch.com/content/titles/en_US/popular?body=${bodyEncoded}`;

  const response = await request({ url, json: true, method: 'GET' });
  if (
    response.total_results
    && response.total_results > 0
    && response.items[0].title.trim().toLowerCase()
      === movieTitle.trim().toLowerCase()
  ) {
    return response.items[0];
  }
  return { notFound: new Date().getTime() };
}

function overrideById({ type, id }) {
  const url = `https://apis.justwatch.com/content/titles/${type}/${id}/locale/en_US`;

  return request({ url, json: true, method: 'GET' });
}

function getJustWatchData(title = '', overrideId = '', objectType = 'movie') {
  const hasOverrideId = overrideId !== 'unset' && !!overrideId;
  return hasOverrideId
    ? overrideById({ id: overrideId, type: objectType })
    : searchByTitle(title);
}

module.exports = {
  getJustWatchData,
};

'use strict';

const queueService = require('queue-shared/queue-service'); // eslint-disable-line import/no-extraneous-dependencies
const { matchPathIdentityId } = require('queue-shared/auth-util'); // eslint-disable-line import/no-extraneous-dependencies

exports.handler = async (event) => {
  const { identityId, authorizationToken } = event;

  if (identityId) {
    if (!authorizationToken) {
      throw new Error('Could not find authToken');
    }
    const token = authorizationToken.split(' ')[1];
    await matchPathIdentityId(token, identityId);
  }

  const id = identityId || process.env.defaultQueueId;
  try {
    const result = await queueService.getQueue(id);
    return {
      statusCode: 200,
      headers: {},
      body: JSON.stringify(result),
      isBase64Encoded: false,
    };
  } catch (e) {
    e.message = `Error: ${e.message}`;
    throw e;
  }
};

AWSTemplateFormatVersion: '2010-09-09'

Parameters:
    DefaultQueueId:
        Type: String
    MainStackName:
        Type: String
    QueueTable:
        Type: String
    QueueVendorLambdaLayer:
        Type: String
    QueueSharedLayer:
        Type: String
    DynamoStackName:
        Type: String
    ApiParentResourceId:
        Type: String
    ApiId:
        Type: String
    CognitoStackName:
        Type: String
    AuthorizerId:
        Type: String

Resources:
    LambdaFn:
        Type: AWS::Lambda::Function
        Properties:
            FunctionName:
                !Join ['', [!Ref 'AWS::StackName', 'GetDefaultQueueFn']]
            Runtime: nodejs8.10
            Handler: index.handler
            Timeout: 10
            Code: .
            Layers:
                - !Ref QueueVendorLambdaLayer
                - !Ref QueueSharedLayer
            Role: !GetAtt LambdaRole.Arn
            Environment:
                Variables:
                    queueTable: !Ref QueueTable
                    defaultQueueId: !Ref DefaultQueueId
                    USER_POOL_ID:
                        Fn::ImportValue: !Sub ${CognitoStackName}::UserPool::Id
                    IDENTITY_POOL_ID:
                        Fn::ImportValue: !Sub ${CognitoStackName}::IdentityPool::Id

    LambdaRole:
        Type: AWS::IAM::Role
        Properties:
            RoleName: !Join ['', [!Ref 'AWS::StackName', 'LambdaFnRole']]
            Path: '/'
            ManagedPolicyArns:
                - arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole
            AssumeRolePolicyDocument:
                Statement:
                    - Action: sts:AssumeRole
                      Effect: Allow
                      Principal:
                          Service:
                              - lambda.amazonaws.com
            Policies:
                - PolicyName: !Sub ${MainStackName}PolicyLambdaAllowGetDefaultQueue
                  PolicyDocument:
                      Version: '2012-10-17'
                      Statement:
                          - Effect: 'Allow'
                            Action:
                                - dynamodb:GetItem
                                - dynamodb:Query
                            Resource:
                                Fn::ImportValue: !Sub ${DynamoStackName}::QueueTable::Arn

    ApiGatewayLambdaPermission:
        Type: AWS::Lambda::Permission
        Properties:
            Action: lambda:InvokeFunction
            FunctionName: !GetAtt LambdaFn.Arn
            Principal: apigateway.amazonaws.com
            SourceArn: !Sub 'arn:aws:execute-api:${AWS::Region}:${AWS::AccountId}:${ApiId}/*/GET/*'

    QueueResource:
        Type: AWS::ApiGateway::Resource
        Properties:
            PathPart: 'queue'
            ParentId: !Ref ApiParentResourceId
            RestApiId: !Ref ApiId

    QueueResourceOptions:
        Type: AWS::ApiGateway::Method
        Properties:
            AuthorizationType: NONE
            ResourceId: !Ref QueueResource
            RestApiId: !Ref ApiId
            HttpMethod: OPTIONS
            Integration:
                IntegrationResponses:
                    - StatusCode: 200
                      ResponseParameters:
                          method.response.header.Access-Control-Allow-Headers: "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,Cache-Control,Pragma'"
                          method.response.header.Access-Control-Allow-Methods: "'GET,OPTIONS'"
                          method.response.header.Access-Control-Allow-Origin: "'*'"
                      ResponseTemplates:
                          application/json: ''
                PassthroughBehavior: WHEN_NO_MATCH
                RequestTemplates:
                    application/json: '{"statusCode": 200}'
                Type: MOCK
            MethodResponses:
                - StatusCode: 200
                  ResponseModels:
                      application/json: Empty
                  ResponseParameters:
                      method.response.header.Access-Control-Allow-Headers: false
                      method.response.header.Access-Control-Allow-Methods: false
                      method.response.header.Access-Control-Allow-Origin: false

    QueueResourceGet:
        Type: AWS::ApiGateway::Method
        Properties:
            AuthorizationType: NONE
            ResourceId: !Ref QueueResource
            RestApiId: !Ref ApiId
            HttpMethod: GET
            Integration:
                IntegrationHttpMethod: 'POST'
                Type: 'AWS'
                PassthroughBehavior: 'WHEN_NO_TEMPLATES'
                RequestTemplates:
                    application/json: |
                        {
                        }
                IntegrationResponses:
                    - StatusCode: 200
                      ResponseTemplates:
                          application/json: '$input.path("$.body")'
                      ResponseParameters:
                          method.response.header.Access-Control-Allow-Headers: "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,Cache-Control,Pragma'"
                          method.response.header.Access-Control-Allow-Methods: "'GET,OPTIONS'"
                          method.response.header.Access-Control-Allow-Origin: "'*'"
                    - StatusCode: 500
                      SelectionPattern: '.*Error.*'
                      ResponseTemplates:
                          application/json: '$input.json("$")'
                      ResponseParameters:
                          method.response.header.Access-Control-Allow-Headers: "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,Cache-Control,Pragma'"
                          method.response.header.Access-Control-Allow-Methods: "'GET,OPTIONS'"
                          method.response.header.Access-Control-Allow-Origin: "'*'"
                Uri: !Sub
                    - 'arn:aws:apigateway:${AWS::Region}:lambda:path/2015-03-31/functions/${lambdaArn}/invocations'
                    - lambdaArn: !GetAtt LambdaFn.Arn
            MethodResponses:
                - StatusCode: 200
                  ResponseModels:
                      application/json: Empty
                  ResponseParameters:
                      method.response.header.Content-Type: true
                      method.response.header.Access-Control-Allow-Headers: true
                      method.response.header.Access-Control-Allow-Methods: true
                      method.response.header.Access-Control-Allow-Origin: true
                - StatusCode: 500
                  ResponseModels:
                      application/json: Empty
                  ResponseParameters:
                      method.response.header.Content-Type: true
                      method.response.header.Access-Control-Allow-Headers: true
                      method.response.header.Access-Control-Allow-Methods: true
                      method.response.header.Access-Control-Allow-Origin: true
    QueueIdResource:
        Type: AWS::ApiGateway::Resource
        Properties:
            PathPart: '{identityId}'
            ParentId: !Ref QueueResource
            RestApiId: !Ref ApiId

    QueueIdResourceOptions:
        Type: AWS::ApiGateway::Method
        Properties:
            AuthorizationType: NONE
            ResourceId: !Ref QueueIdResource
            RestApiId: !Ref ApiId
            HttpMethod: OPTIONS
            Integration:
                IntegrationResponses:
                    - StatusCode: 200
                      ResponseParameters:
                          method.response.header.Access-Control-Allow-Headers: "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,Cache-Control,Pragma'"
                          method.response.header.Access-Control-Allow-Methods: "'GET,OPTIONS'"
                          method.response.header.Access-Control-Allow-Origin: "'*'"
                      ResponseTemplates:
                          application/json: ''
                PassthroughBehavior: WHEN_NO_MATCH
                RequestTemplates:
                    application/json: '{"statusCode": 200}'
                Type: MOCK
            MethodResponses:
                - StatusCode: 200
                  ResponseModels:
                      application/json: Empty
                  ResponseParameters:
                      method.response.header.Access-Control-Allow-Headers: false
                      method.response.header.Access-Control-Allow-Methods: false
                      method.response.header.Access-Control-Allow-Origin: false

    QueueIdResourceGet:
        Type: AWS::ApiGateway::Method
        Properties:
            AuthorizationType: COGNITO_USER_POOLS
            AuthorizerId: !Ref AuthorizerId
            ResourceId: !Ref QueueIdResource
            RestApiId: !Ref ApiId
            RequestValidatorId: !Ref RequestValidator
            HttpMethod: GET
            RequestParameters:
                method.request.path.identityId: true
                method.request.header.Authorization: true
            Integration:
                IntegrationHttpMethod: POST
                Type: AWS
                PassthroughBehavior: WHEN_NO_TEMPLATES
                RequestTemplates:
                    application/json: |
                        {
                            "identityId": "$input.params('identityId')",
                            "authorizationToken": "$input.params('Authorization')"
                        }
                IntegrationResponses:
                    - StatusCode: 200
                      ResponseTemplates:
                          application/json: '$input.path("$.body")'
                      ResponseParameters:
                          method.response.header.Access-Control-Allow-Headers: "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,Cache-Control,Pragma'"
                          method.response.header.Access-Control-Allow-Methods: "'GET,OPTIONS'"
                          method.response.header.Access-Control-Allow-Origin: "'*'"
                    - StatusCode: 500
                      SelectionPattern: '.*Error.*'
                      ResponseTemplates:
                          application/json: '$input.json("$")'
                      ResponseParameters:
                          method.response.header.Access-Control-Allow-Headers: "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,Cache-Control,Pragma'"
                          method.response.header.Access-Control-Allow-Methods: "'GET,OPTIONS'"
                          method.response.header.Access-Control-Allow-Origin: "'*'"
                Uri: !Sub
                    - 'arn:aws:apigateway:${AWS::Region}:lambda:path/2015-03-31/functions/${lambdaArn}/invocations'
                    - lambdaArn: !GetAtt LambdaFn.Arn
            MethodResponses:
                - StatusCode: 200
                  ResponseModels:
                      application/json: Empty
                  ResponseParameters:
                      method.response.header.Content-Type: true
                      method.response.header.Access-Control-Allow-Headers: true
                      method.response.header.Access-Control-Allow-Methods: true
                      method.response.header.Access-Control-Allow-Origin: true
                - StatusCode: 500
                  ResponseModels:
                      application/json: Empty
                  ResponseParameters:
                      method.response.header.Content-Type: true
                      method.response.header.Access-Control-Allow-Headers: true
                      method.response.header.Access-Control-Allow-Methods: true
                      method.response.header.Access-Control-Allow-Origin: true

    RequestValidator:
        Type: AWS::ApiGateway::RequestValidator
        Properties:
            Name: !Sub ${AWS::StackName}-ApiGateway-QueueIdResourceGetValidator
            RestApiId: !Ref ApiId
            ValidateRequestBody: false
            ValidateRequestParameters: true

Outputs:
    QueueIdResourceId:
        Value: !Ref QueueIdResource
        Export:
            Name: !Sub ${AWS::StackName}::QueueIdResource::Id

    StackName:
        Value: !Ref AWS::StackName

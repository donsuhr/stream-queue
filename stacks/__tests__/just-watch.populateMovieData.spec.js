/* eslint-env jest */

import AWS from 'aws-sdk';
import {
  populateMovieData,
  updateJustWatch,
  cleanJustWatchResponse,
} from '../lambda/just-watch/populateMovieData';

jest.mock('aws-sdk');

let queueServiceGetResult;
const queueService = {
  getQueue: () => Promise.resolve(queueServiceGetResult),
  updateJustWatch: () => Promise.resolve(),
};
const justWatchService = {
  getJustWatchData: () => Promise.resolve({}),
};

describe('lambda just-watch populateMovieData', () => {
  beforeEach(() => {
    // sut = createParsedMovieStore({});
    queueService.getQueue = () => Promise.resolve(queueServiceGetResult);
    queueServiceGetResult = [{ netflixId: 123 }];
  });

  it('calls', async () => {
    await expect(
      populateMovieData({
        queueService,
        justWatchService,
        netflixId: 123,
        identityId: 234,
        overrideId: 345,
      }),
    ).resolves.not.toThrow();
  });

  it('throws no queue error', async () => {
    queueService.getQueue = () => Promise.reject(new Error('queueItems property not set'));

    await expect(
      populateMovieData({
        queueService,
        netflixId: 123,
        identityId: 234,
        overrideId: 345,
      }),
    ).rejects.toThrow('User has no queue');
  });

  it('throws movie not in queue error', async () => {
    queueServiceGetResult = [];
    await expect(
      populateMovieData({
        queueService,
        netflixId: 123,
        identityId: 234,
        overrideId: 345,
      }),
    ).rejects.toThrow(/not found/);
  });

  describe('updateJustWatch', () => {
    it('calls', async () => {
      AWS.DocClientUpdateFn.mockClear();
      await updateJustWatch({
        identityId: 22,
        netflixId: 1,
        justwatchResponse: {},
        justwatchId: 23,
      });
      expect(AWS.DocClientUpdateFn.mock.calls[0][0].Key).toEqual({
        identityId: 22,
        netflixId: 1,
      });
    });

    it('removes the overrideId if passed "unset"', async () => {
      AWS.DocClientUpdateFn.mockClear();
      await updateJustWatch({
        identityId: 22,
        netflixId: 1,
        justwatchResponse: {},
        justwatchId: 'unset',
      });
      expect(AWS.DocClientUpdateFn.mock.calls[0][0].UpdateExpression).toContain(
        'REMOVE justwatchId',
      );
    });
  });

  describe('cleanJustWatchResponse', () => {
    it('deletes empty string nodes', () => {
      const obj = { target: '', nonTarget: 'foo' };
      const result = cleanJustWatchResponse(obj);
      expect(Object.keys(result)).not.toContain('target');
    });

    it('deletes nested empty string nodes', () => {
      const obj = {
        target1: { target2: '', nonTarget2: 'bar' },
        nonTarget: 'foo',
      };
      const result = cleanJustWatchResponse(obj);
      expect(Object.keys(result.target1)).not.toContain('target2');
      expect(Object.keys(result.target1)).toContain('nonTarget2');
    });
  });
});

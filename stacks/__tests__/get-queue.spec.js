/* eslint-env jest */
import { getQueue } from 'queue-shared/queue-service'; // eslint-disable-line import/no-extraneous-dependencies
import { matchPathIdentityId } from 'queue-shared/auth-util'; // eslint-disable-line import/no-extraneous-dependencies

jest.mock('queue-shared/queue-service');
jest.mock('queue-shared/auth-util');
const sut = require('../lambda/get-queue').handler;

describe('get-queue-spec', () => {
  beforeEach(() => {
    getQueue.mockClear();
  });

  it('calls', async () => {
    await expect(sut({})).resolves.not.toThrow();
  });

  it('throws when auth missing', async () => {
    await expect(sut({ identityId: 2 })).rejects.toThrow();
  });

  it('uses defaultQueueId when id is missing', async () => {
    const id = '22';
    process.env.defaultQueueId = id;
    await sut({});
    expect(getQueue).toHaveBeenCalledTimes(1);
    expect(getQueue).toHaveBeenCalledWith(id);
  });

  it('throws an error with the word "Error"', async () => {
    matchPathIdentityId.mockImplementation(() => {
      throw new Error('Error: foo');
    });
    await expect(
      sut({ identityId: 2, authorizationToken: 'abc' }),
    ).rejects.toThrow('Error');
  });
});

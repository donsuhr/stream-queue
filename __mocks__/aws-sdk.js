/* eslint-env jest */

'use strict';

let DocClientGetFnResult = { Items: [] };
const DocClientGetFn = jest.fn().mockImplementation(() => ({
  promise: () => Promise.resolve(DocClientGetFnResult),
}));

const DocClientUpdateFn = jest.fn().mockImplementation(() => ({
  promise: () => Promise.resolve(DocClientGetFnResult),
}));

module.exports = {
  CognitoIdentityCredentials: jest.fn(),
  config: {
    update: jest.fn(),
    credentials: {
      get: jest.fn().mockImplementation((cb) => {
        cb();
      }),
    },
  },
  DynamoDB: {
    DocumentClient: jest.fn().mockImplementation(() => ({
      query: DocClientGetFn,
      update: DocClientUpdateFn,
    })),
  },
  DocClientGetFn,
  DocClientUpdateFn,
  SetDocClientGetFnResult(result) {
    DocClientGetFnResult = result;
  },
};

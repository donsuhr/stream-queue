/* global serviceWorkerVersion, serviceWorkerOption, process */
/* eslint-env serviceworker */

const VERSION = serviceWorkerVersion;
const offlineImage = `<svg role="img" aria-labelledby="offline-title"
                        viewBox="0 0 400 300" xmlns="http://www.w3.org/2000/svg">
                        <title id="offline-title">Offline</title>
                        <g fill="none" fill-rule="evenodd"><path fill="#D8D8D8" d="M0 0h400v300H0z"/>
                        <text fill="#9B9B9B" font-family="Times New Roman,Times,serif" font-size="72" font-weight="bold">
                        <tspan x="93" y="172">offline</tspan></text></g>
                    </svg>`;

const { assets } = serviceWorkerOption;

const offlinePage = '/offline/';

function addToCache(cacheKey, request, response) {
  return new Promise((res) => {
    if (response.ok) {
      const copy = response.clone();
      caches
        .open(cacheKey)
        .then((cache) => cache.put(request, copy))
        .then(() => {
          res(response);
        });
    } else {
      res(response);
    }
  });
}

function fetchFromCache(request) {
  return caches.match(request).then((response) => {
    if (!response) {
      // A synchronous error that will kick off the catch handler
      throw Error(`${request.url} not found in cache`);
    }
    return response;
  });
}

function offlineResponse(resourceType) {
  if (resourceType === 'image') {
    return new Response(offlineImage, {
      headers: { 'Content-Type': 'image/svg+xml' },
    });
  }
  if (resourceType === 'content') {
    return caches.match(offlinePage);
  }
  if (resourceType === 'queue-json') {
    return new Response('{"error": "offline"}', {
      headers: { 'Content-Type': 'application/json' },
    });
  }
  return undefined;
}

function fetchFromNetwork(request, cacheKey) {
  return fetch(request).then((response) => addToCache(cacheKey, request, response));
}

function determineResourceType(request) {
  const acceptHeader = request.headers.get('Accept');
  if (acceptHeader.indexOf('text/html') !== -1) {
    return 'content';
  }
  if (acceptHeader.indexOf('image') !== -1) {
    return 'image';
  }
  return 'static';
}

function refresh(response) {
  // eslint-disable-next-line no-restricted-globals
  return self.clients.matchAll().then((clients) => {
    clients.forEach((client) => {
      const message = {
        type: 'refresh',
        url: response.url,
        eTag: response.headers.get('ETag'),
      };
      client.postMessage(JSON.stringify(message));
    });
    return response;
  });
}

// eslint-disable-next-line no-restricted-globals
self.addEventListener('install', (event) => {
  function onInstall() {
    const rootPath = process.env.NODE_ENV === 'production' ? '/projects/queue/' : '/';
    return caches
      .open(`${VERSION}`)
      .then((cache) => cache.addAll([rootPath, ...assets]))
      .then(() => {
        // use this or a refresh button in index.js
        // eslint-disable-next-line no-restricted-globals
        self.skipWaiting();
      });
  }

  event.waitUntil(onInstall(event));
});

// eslint-disable-next-line no-restricted-globals
self.addEventListener('fetch', (event) => {
  /* eslint-disable consistent-return */
  const { request } = event;
  if (request.method !== 'GET') {
    return;
  }
  if (request.url.match(/\/queue$/i)) {
    event.respondWith(
      fetchFromCache(event.request, VERSION)
        .then((cacheResponse) => {
          fetchFromNetwork(event.request, VERSION).then((response) => refresh(response));
          return cacheResponse;
        })
        .catch(() => fetchFromNetwork(event.request, VERSION))
        .catch(() => offlineResponse('queue-json')),
    );

    return;
  }
  if (request.url.match('assets.nflxext.com')) {
    return event.respondWith(
      fetch(request).catch((e) => offlineResponse('image')),
    );
  }

  const requestUrl = new URL(request.url);
  // eslint-disable-next-line no-restricted-globals
  if (requestUrl.origin !== location.origin) {
    return;
  }

  if (
    request.url.match(/hot-update/)
        || request.url.match(/__webpack_hmr/)
        || request.url.match(/\/sockjs-node\/info/)
  ) {
    return;
  }

  return event.respondWith(
    fetchFromCache(event.request, VERSION)
      .catch(() => fetchFromNetwork(event.request, VERSION))
      .catch(() => offlineResponse(determineResourceType(request))),
  );
});

// eslint-disable-next-line no-restricted-globals
self.addEventListener('activate', (event) => {
  event.waitUntil(
    caches
      .keys()
      .then((cacheKeys) => Promise.all(
        cacheKeys
          .filter((key) => key.indexOf(VERSION) !== 0)
          .map((oldKey) => caches.delete(oldKey)),
      )),
  );
});

// eslint-disable-next-line no-restricted-globals
self.addEventListener('message', (event) => {
  if (event.data.action === 'skipWaiting') {
    // eslint-disable-next-line no-restricted-globals
    self.skipWaiting();
  }
});

import { shallowMount, createLocalVue } from '@vue/test-utils';
import Router from 'vue-router';
import ForgotPassword from '../ForgotPassword';
import confirmCodeSuccess from '../../service/fixtures/forgot-password-success.json';
import resourceError from '../../service/fixtures/forgot-password-resource-error.json';
import invalidParamError from '../../service/fixtures/forgot-password-invalid-param.json';
import unknownEmailError from '../../service/fixtures/forgot-password-not-found.json';
import { LS_KEY_SIGNUP_EMAIL } from '../../service/auth-service-const';

import * as authService from '../../service/auth-service';

let localVue;

describe('Forgot Password Email Page', () => {
  beforeEach(() => {
    localVue = createLocalVue();
    localVue.use(Router);
    authService.forgotPassword = jest
      .fn()
      .mockImplementation(() => Promise.reject(confirmCodeSuccess));
  });

  it('renders', () => {
    const sut = shallowMount(ForgotPassword, {
      localVue,
      propsData: {
        authService,
      },
    });
    expect(sut.find('.forgot-password-page h1').text()).toContain(
      'Forgot Password',
    );
  });

  it('loads email from localstorage', () => {
    const testEmail = 'valid@email.com';
    localStorage.setItem(LS_KEY_SIGNUP_EMAIL, testEmail);
    const sut = shallowMount(ForgotPassword, {
      localVue,
      propsData: {
        authService,
      },
    });
    expect(sut.find('#email').element.value).toBe(testEmail);
  });

  it('shows invalid email error', async () => {
    const sut = shallowMount(ForgotPassword, {
      localVue,
      propsData: {
        authService,
      },
    });
    sut.vm.email = 'invalid-email.com';
    sut.find('form').trigger('submit');
    await localVue.nextTick();
    await localVue.nextTick();
    sut.vm.$forceUpdate();
    expect(
      sut
        .find('.input-error-list')
        .text()
        .toLowerCase(),
    ).toContain('enter a valid email');
  });

  it('shows a resource error', async () => {
    authService.forgotPassword = jest
      .fn()
      .mockImplementation(() => Promise.reject(resourceError));
    const sut = shallowMount(ForgotPassword, {
      localVue,
      propsData: {
        authService,
      },
    });
    sut.vm.email = 'valid@email.com';
    sut.find('form').trigger('submit');
    await localVue.nextTick();
    await localVue.nextTick();
    sut.vm.$forceUpdate();
    expect(
      sut
        .find('.forgot-password-page .error')
        .text()
        .toLowerCase(),
    ).toContain('[config]');
  });

  it('shows a server invalid email', async () => {
    authService.forgotPassword = jest
      .fn()
      .mockImplementation(() => Promise.reject(invalidParamError));
    const sut = shallowMount(ForgotPassword, {
      localVue,
      propsData: {
        authService,
      },
    });
    sut.vm.email = 'valid@email.com';
    sut.find('form').trigger('submit');
    await localVue.nextTick();
    await localVue.nextTick();
    sut.vm.$forceUpdate();
    expect(
      sut
        .find('.input-error-list')
        .text()
        .toLowerCase(),
    ).toContain('server was unable to validate');
  });

  it('shows a server unknown email', async () => {
    authService.forgotPassword = jest
      .fn()
      .mockImplementation(() => Promise.reject(unknownEmailError));
    const sut = shallowMount(ForgotPassword, {
      localVue,
      propsData: {
        authService,
      },
    });
    sut.vm.email = 'valid@email.com';
    sut.find('form').trigger('submit');
    await localVue.nextTick();
    await localVue.nextTick();
    sut.vm.$forceUpdate();
    expect(
      sut
        .find('.forgot-password-page .error')
        .text()
        .toLowerCase(),
    ).toContain('not found');
  });
});

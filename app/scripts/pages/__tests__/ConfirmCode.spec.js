import { shallowMount, createLocalVue } from '@vue/test-utils';
import Router from 'vue-router';
import ConfirmCode from '../ConfirmCode';
import invalidParamError from '../../service/fixtures/confirm-code-invalid-param.json';
import invalidCodeError from '../../service/fixtures/confirm-code-invalid-code.json';
import confirmCodeSuccess from '../../service/fixtures/confirm-code-success.json';
import resourceError from '../../service/fixtures/confirm-code-resource-not-found.json';
import { LS_KEY_SIGNUP_EMAIL } from '../../service/auth-service-const';

import * as authService from '../../service/auth-service';

let localVue;

describe('Confirm Page', () => {
  beforeEach(() => {
    localVue = createLocalVue();
    localVue.use(Router);
    authService.confirmCode = jest
      .fn()
      .mockImplementation(() => Promise.reject(invalidParamError));
  });

  it('renders', () => {
    const sut = shallowMount(ConfirmCode, {
      localVue,
      propsData: {
        authService,
      },
    });
    expect(sut.find('.confirm-code-page h1').text()).toContain(
      'Confirm Code',
    );
  });

  it('loads email from localstorage', () => {
    const testEmail = 'valid@email.com';
    localStorage.setItem(LS_KEY_SIGNUP_EMAIL, testEmail);
    const sut = shallowMount(ConfirmCode, {
      localVue,
      propsData: {
        authService,
      },
    });
    expect(sut.find('#email').element.value).toBe(testEmail);
  });

  it('shows wrong code error', async () => {
    authService.confirmCode = jest
      .fn()
      .mockImplementation(() => Promise.reject(invalidCodeError));
    const sut = shallowMount(ConfirmCode, {
      localVue,
      propsData: {
        authService,
      },
    });
    sut.find('form').trigger('submit');
    await localVue.nextTick();
    await localVue.nextTick();
    sut.vm.$forceUpdate();
    expect(
      sut
        .find('.confirm-code-page .error p')
        .text()
        .toLowerCase(),
    ).toContain('invalid');
  });

  it('shows wrong code error', async () => {
    authService.confirmCode = jest
      .fn()
      .mockImplementation(() => Promise.reject(resourceError));
    const sut = shallowMount(ConfirmCode, {
      localVue,
      propsData: {
        authService,
      },
    });
    sut.find('form').trigger('submit');
    await localVue.nextTick();
    await localVue.nextTick();
    sut.vm.$forceUpdate();
    expect(
      sut
        .find('.confirm-code-page .error p')
        .text()
        .toLowerCase(),
    ).toContain('error');
  });

  it('it shows a server error for invalid input', async () => {
    const sut = shallowMount(ConfirmCode, {
      localVue,
      propsData: {
        authService,
      },
    });
    sut.find('form').trigger('submit');
    await localVue.nextTick();
    await localVue.nextTick();
    sut.vm.$forceUpdate();
    expect(
      sut
        .findAll('#email + .input-error-list li')
        .at(0)
        .text()
        .toLowerCase(),
    ).toContain('server');
    expect(
      sut
        .findAll('#code + .input-error-list li')
        .at(0)
        .text()
        .toLowerCase(),
    ).toContain('server');
  });

  it('redirects to login code after success', async () => {
    authService.confirmCode = jest
      .fn()
      .mockImplementation(() => Promise.resolve(confirmCodeSuccess));
    const router = new Router();
    router.push = jest.fn();
    const sut = shallowMount(ConfirmCode, {
      localVue,
      router,
      propsData: {
        authService,
      },
    });
    sut.vm.email = 'valid@email.com';
    sut.vm.code = '1234';
    sut.find('form').trigger('submit');
    await localVue.nextTick();
    await localVue.nextTick();
    sut.vm.$forceUpdate();
    expect(sut.vm.$router.push).toHaveBeenCalledWith('login');
  });
});

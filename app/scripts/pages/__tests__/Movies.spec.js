import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import Movies from '../Movies';

jest.mock('../../components/queue/queue-grid');

let store;
let localVue;

const items = [
  {
    _id: '5a85ea1206f3df5a696bc34f',
    netflixId: '80116741',
    title: 'My Life as a Zucchini',
    updatedAt: '2018-02-17T05:29:40.492Z',
  },
  {
    _id: '5a85ea1206f3df5a696bc34g',
    netflixId: '801167412',
    title: 'My Life as a Zucchini2',
    updatedAt: '2018-02-17T05:29:40.492Z',
  },
];

describe('Movies Page', () => {
  beforeEach(() => {
    localVue = createLocalVue();
    localVue.use(Vuex);
    store = new Vuex.Store({
      state: {},
    });
    store.getters = {
      'queue/items': items,
      'queue/unpopulatedItems': [],
      'queue/streamableItems': [],
      'queue/rentableItems': [],
      'queue/nothingItems': [],
      'queue/notFoundItems': [],
    };
  });

  it('renders', () => {
    const $route = {
      query: { noreload: 'all' },
      params: { filter: 'all' },
    };
    const sut = shallowMount(Movies, {
      localVue,
      store,
      mocks: {
        $route,
      },
      stubs: ['RouterLink'],
    });
    expect(sut.find('section').attributes('class')).toContain(
      'movies-page',
    );
  });

  it('toggles grid view', () => {
    const $route = {
      query: { noreload: 'all' },
      params: { filter: 'all' },
    };
    store.getters['ui/isGridView'] = false;

    let sut = shallowMount(Movies, {
      localVue,
      store,
      mocks: {
        $route,
      },
      stubs: ['RouterLink'],
    });
    expect(sut.findAll('queuelist-stub').length).toBe(1);
    expect(sut.findAll('queuegrid-stub').length).toBe(0);
    store.getters['ui/isGridView'] = true;

    sut = shallowMount(Movies, {
      localVue,
      store,
      mocks: {
        $route,
      },
      stubs: ['RouterLink', 'QueueList', 'QueueGrid'],
    });
    expect(sut.findAll('queuelist-stub').length).toBe(0);
    expect(sut.findAll('queuegrid-stub').length).toBe(1);
  });

  it('shows filter when viewing canstream', async () => {
    const $route = {
      query: { noreload: 'all' },
      params: { filter: 'canstream' },
    };
    store.replaceState({
      queue: {
        items,
      },
    });

    const sut = shallowMount(Movies, {
      store,
      localVue,
      mocks: {
        $route,
      },
      stubs: ['RouterLink'],
    });
    await localVue.nextTick();
    expect(sut.find('.provider-list').exists()).toBe(true);
  });

  it('shows filter when viewing canrent', async () => {
    const $route = {
      query: { noreload: 'all' },
      params: { filter: 'canrent' },
    };
    store.replaceState({
      queue: {
        items,
      },
    });

    const sut = shallowMount(Movies, {
      store,
      localVue,
      mocks: {
        $route,
      },
      stubs: ['RouterLink'],
    });
    await localVue.nextTick();
    expect(sut.find('.provider-list').exists()).toBe(true);
  });
});

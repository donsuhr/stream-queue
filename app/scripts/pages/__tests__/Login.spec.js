import { shallowMount, createLocalVue } from '@vue/test-utils';
import Router from 'vue-router';
import Login from '../Login';

import invalidResource from '../../service/fixtures/login-resource-not-found.json';
import invalidParam from '../../service/fixtures/login-invalid-param.json';
import wrongError from '../../service/fixtures/login-mismatch.json';
import unknownUserError from '../../service/fixtures/login-user-not-found.json';
import notConfirmedError from '../../service/fixtures/login-not-confirmed.json';

import * as authService from '../../service/auth-service';

let localVue;

describe('Login Page', () => {
  beforeEach(() => {
    localVue = createLocalVue();
    localVue.use(Router);

    authService.login = jest
      .fn()
      .mockImplementation(() => Promise.reject(invalidParam));
  });

  it('renders', () => {
    const sut = shallowMount(Login, {
      localVue,
      propsData: {
        authService,
      },
    });
    expect(sut.find('.login-page h1').text()).toContain('Login');
  });

  it('redirects after login', async () => {
    authService.login = jest.fn().mockImplementation(() => Promise.resolve());
    const router = new Router();
    router.push = jest.fn();
    const sut = shallowMount(Login, {
      localVue,
      router,
      propsData: {
        authService,
      },
    });
    sut.vm.$v.email.$model = 'valid@email.com';
    sut.vm.$v.password.$model = '123456';
    sut.find('form').trigger('submit');
    await localVue.nextTick();
    await localVue.nextTick();
    expect(sut.vm.$router.push).toHaveBeenCalledWith('queue');
  });

  it('it calls though to login', async () => {
    const sut = shallowMount(Login, {
      localVue,
      propsData: {
        authService,
      },
    });

    sut.vm.$v.email.$model = 'valid@email.com';
    sut.vm.$v.password.$model = '123456';
    sut.find('form').trigger('submit');
    expect(authService.login.mock.calls).toHaveLength(1);
  });

  it('it shows error for invalid input', async () => {
    const sut = shallowMount(Login, {
      localVue,
      propsData: {
        authService,
      },
    });
    sut.vm.$v.email.$model = 'valid@email.com';
    sut.vm.$v.password.$model = '123456';
    sut.find('form').trigger('submit');
    await localVue.nextTick();
    await localVue.nextTick();
    expect(
      sut
        .find('.login-page .error p')
        .text()
        .toLowerCase(),
    ).toContain('invalid');
  });

  it('it shows a config error', async () => {
    authService.login = jest
      .fn()
      .mockImplementation(() => Promise.reject(invalidResource));

    const sut = shallowMount(Login, {
      localVue,
      propsData: {
        authService,
      },
    });
    sut.vm.$v.email.$model = 'valid@email.com';
    sut.vm.$v.password.$model = '123456';
    sut.find('form').trigger('submit');
    await localVue.nextTick();
    await localVue.nextTick();
    expect(
      sut
        .find('.login-page .error p')
        .text()
        .toLowerCase(),
    ).toContain('config');
  });

  it('it shows a server error for invalid input', async () => {
    const sut = shallowMount(Login, {
      localVue,
      propsData: {
        authService,
      },
    });
    sut.vm.$v.email.$model = 'valid@email.com';
    sut.vm.$v.password.$model = '123456';
    sut.find('form').trigger('submit');
    await localVue.nextTick();
    await localVue.nextTick();
    sut.vm.$forceUpdate();

    expect(
      sut
        .findAll('#email + .input-error-list li')
        .at(0)
        .text()
        .toLowerCase(),
    ).toContain('server');
  });

  it('shows client side validation error', async () => {
    const sut = shallowMount(Login, {
      localVue,
      propsData: {
        authService,
      },
    });
    sut.vm.$v.email.$model = 'invalid';
    sut.vm.$v.password.$model = '';
    sut.find('form').trigger('submit');
    await localVue.nextTick();
    await localVue.nextTick();
    sut.vm.$forceUpdate();
    expect(
      sut
        .findAll('#email + .input-error-list li')
        .at(0)
        .text()
        .toLowerCase(),
    ).toContain('email');
    expect(
      sut
        .findAll('#password + .input-error-list li')
        .at(0)
        .text()
        .toLowerCase(),
    ).toContain('password');
  });

  it('shows an error for an unconfirmed users', async () => {
    authService.login = jest
      .fn()
      .mockImplementation(() => Promise.reject(notConfirmedError));

    const sut = shallowMount(Login, {
      localVue,
      propsData: {
        authService,
      },
    });
    sut.vm.$v.email.$model = 'valid@email.com';
    sut.vm.$v.password.$model = '123456';
    sut.find('form').trigger('submit');
    await localVue.nextTick();
    await localVue.nextTick();
    expect(
      sut
        .find('.login-page .error p')
        .text()
        .toLowerCase(),
    ).toContain('confirmed');
    // show link
    expect(
      sut
        .findAll('routerlink-stub')
        .at(2)
        .text()
        .toLowerCase(),
    ).toContain('confirm');
  });

  it('shows an error for wrong user pass', async () => {
    authService.login = jest
      .fn()
      .mockImplementation(() => Promise.reject(wrongError));

    const sut = shallowMount(Login, {
      localVue,
      propsData: {
        authService,
      },
    });
    sut.vm.$v.email.$model = 'valid@email.com';
    sut.vm.$v.password.$model = '123456';
    sut.find('form').trigger('submit');
    await localVue.nextTick();
    await localVue.nextTick();
    expect(
      sut
        .find('.login-page .error p')
        .text()
        .toLowerCase(),
    ).toContain('incorrect');
  });

  it('shows an error for wrong user ', async () => {
    authService.login = jest
      .fn()
      .mockImplementation(() => Promise.reject(unknownUserError));

    const sut = shallowMount(Login, {
      localVue,
      propsData: {
        authService,
      },
    });
    sut.vm.$v.email.$model = 'valid@email.com';
    sut.vm.$v.password.$model = '123456';
    sut.find('form').trigger('submit');
    await localVue.nextTick();
    await localVue.nextTick();
    expect(
      sut
        .find('.login-page .error p')
        .text()
        .toLowerCase(),
    ).toContain('incorrect');
  });
});

import { shallowMount, createLocalVue } from '@vue/test-utils';
import Router from 'vue-router';
import SignUp from '../SignUp';
import invalidError from '../../service/fixtures/sign-up-invalid-param.json';
import resourceError from '../../service/fixtures/sign-up-resource-not-found.json';
import signUpSuccess from '../../service/fixtures/sign-up-success.json';
import { LS_KEY_SIGNUP_EMAIL } from '../../service/auth-service-const';

import * as authService from '../../service/auth-service';

let localVue;

describe('Signup Page', () => {
  beforeEach(() => {
    localVue = createLocalVue();
    localVue.use(Router);

    authService.signUp = jest
      .fn()
      .mockImplementation(() => Promise.reject(invalidError));
  });

  it('renders', () => {
    const sut = shallowMount(SignUp, {
      localVue,
      propsData: {
        authService,
      },
    });
    expect(sut.find('.signup-page h1').text()).toContain('Sign Up');
  });

  it('it calls though to signUp', async () => {
    const sut = shallowMount(SignUp, {
      localVue,
      propsData: {
        authService,
      },
    });

    sut.vm.$v.email.$model = 'valid@email.com';
    sut.vm.$v.password.$model = '123456';
    sut.vm.$v.confirmPassword.$model = '123456';
    sut.find('form').trigger('submit');
    expect(authService.signUp.mock.calls).toHaveLength(1);
  });

  it('it shows error for invalid input', async () => {
    const sut = shallowMount(SignUp, {
      localVue,
      propsData: {
        authService,
      },
    });
    sut.vm.$v.email.$model = 'valid@email.com';
    sut.vm.$v.password.$model = '123456';
    sut.vm.$v.confirmPassword.$model = '123456';
    sut.find('form').trigger('submit');
    await localVue.nextTick();
    await localVue.nextTick();
    expect(
      sut
        .find('.signup-page .error p')
        .text()
        .toLowerCase(),
    ).toContain('invalid');
    expect(sut.findAll('.signup-page .errors li').length).toBe(6);
  });

  it('it shows a config error', async () => {
    authService.signUp = jest
      .fn()
      .mockImplementation(() => Promise.reject(resourceError));

    const sut = shallowMount(SignUp, {
      localVue,
      propsData: {
        authService,
      },
    });
    sut.vm.$v.email.$model = 'valid@email.com';
    sut.vm.$v.password.$model = '123456';
    sut.vm.$v.confirmPassword.$model = '123456';
    sut.find('form').trigger('submit');
    await localVue.nextTick();
    await localVue.nextTick();
    expect(
      sut
        .find('.signup-page .error p')
        .text()
        .toLowerCase(),
    ).toContain('config');
  });

  it('it shows a server error for invalid input', async () => {
    const sut = shallowMount(SignUp, {
      localVue,
      propsData: {
        authService,
      },
    });
    sut.vm.$v.email.$model = 'valid@email.com';
    sut.vm.$v.password.$model = '123456';
    sut.vm.$v.confirmPassword.$model = '123456';
    sut.find('form').trigger('submit');
    await localVue.nextTick();
    await localVue.nextTick();
    sut.vm.$forceUpdate();

    expect(
      sut
        .findAll('#email + .input-error-list li')
        .at(0)
        .text()
        .toLowerCase(),
    ).toContain('server');
    expect(
      sut
        .findAll('#password + .input-error-list li')
        .at(0)
        .text()
        .toLowerCase(),
    ).toContain('server');
  });

  it('shows client side validation error', async () => {
    const sut = shallowMount(SignUp, {
      localVue,
      propsData: {
        authService,
      },
    });
    sut.vm.$v.email.$model = 'invalid';
    sut.vm.$v.password.$model = '123456';
    sut.find('form').trigger('submit');
    await localVue.nextTick();
    await localVue.nextTick();
    sut.vm.$forceUpdate();
    expect(
      sut
        .findAll('#email + .input-error-list li')
        .at(0)
        .text()
        .toLowerCase(),
    ).toContain('email');
  });

  it('saves the email used to sign up', async () => {
    authService.signUp = jest
      .fn()
      .mockImplementation(() => Promise.resolve(signUpSuccess));
    const sut = shallowMount(SignUp, {
      localVue,
      propsData: {
        authService,
      },
    });
    const testEmail = 'valid@email.com';
    sut.vm.$v.email.$model = testEmail;
    sut.vm.$v.password.$model = '123456';
    sut.vm.$v.confirmPassword.$model = '123456';
    sut.find('form').trigger('submit');
    await localVue.nextTick();
    await localVue.nextTick();
    sut.vm.$forceUpdate();

    expect(localStorage.setItem).toHaveBeenLastCalledWith(
      LS_KEY_SIGNUP_EMAIL,
      testEmail,
    );
  });

  it('redirects to confirm code after success', async () => {
    authService.signUp = jest
      .fn()
      .mockImplementation(() => Promise.resolve(signUpSuccess));
    const router = new Router();
    router.push = jest.fn();
    const sut = shallowMount(SignUp, {
      localVue,
      router,
      propsData: {
        authService,
      },
    });
    sut.vm.$v.email.$model = 'valid@email.com';
    sut.vm.$v.password.$model = '123456';
    sut.vm.$v.confirmPassword.$model = '123456';
    sut.find('form').trigger('submit');
    await localVue.nextTick();
    await localVue.nextTick();
    sut.vm.$forceUpdate();
    expect(sut.vm.$router.push).toHaveBeenCalledWith('confirm');
  });
});

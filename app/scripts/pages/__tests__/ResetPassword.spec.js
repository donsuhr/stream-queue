import { shallowMount, createLocalVue } from '@vue/test-utils';
import Router from 'vue-router';
import ResetPassword from '../ResetPassword';
import expiredError from '../../service/fixtures/reset-password-expired.json';
import invalidParamError from '../../service/fixtures/reset-password-invalid-param.json';
import resourceError from '../../service/fixtures/reset-password-resource-error.json';

import { LS_KEY_SIGNUP_EMAIL } from '../../service/auth-service-const';

import * as authService from '../../service/auth-service';

let localVue;

describe('Reset Password Page', () => {
  beforeEach(() => {
    localVue = createLocalVue();
    localVue.use(Router);
    authService.resetPassword = jest
      .fn()
      .mockImplementation(() => Promise.reject(resourceError));
  });

  it('renders', () => {
    const sut = shallowMount(ResetPassword, {
      localVue,
      propsData: {
        authService,
      },
    });
    expect(sut.find('.reset-password-page h1').text()).toContain(
      'Reset Password',
    );
  });

  it('loads email from localstorage', () => {
    const testEmail = 'valid@email.com';
    localStorage.setItem(LS_KEY_SIGNUP_EMAIL, testEmail);
    const sut = shallowMount(ResetPassword, {
      localVue,
      propsData: {
        authService,
      },
    });
    expect(sut.find('#email').element.value).toBe(testEmail);
  });

  it('redirects to login code after success', async () => {
    authService.resetPassword = jest
      .fn()
      .mockImplementation(() => Promise.resolve({}));
    const router = new Router();
    router.push = jest.fn();
    const sut = shallowMount(ResetPassword, {
      localVue,
      router,
      propsData: {
        authService,
      },
    });
    sut.vm.email = 'valid@email.com';
    sut.vm.code = '1234';
    sut.vm.$v.newPassword.$model = '123456';
    sut.vm.$v.confirmPassword.$model = '123456';
    sut.find('form').trigger('submit');
    await localVue.nextTick();
    // sut.vm.$forceUpdate();
    expect(sut.vm.$router.push).toHaveBeenCalledWith('login');
  });

  it('shows an invalid code error', async () => {
    authService.resetPassword = jest
      .fn()
      .mockImplementation(() => Promise.reject(expiredError));
    const router = new Router();
    router.push = jest.fn();
    const sut = shallowMount(ResetPassword, {
      localVue,
      router,
      propsData: {
        authService,
      },
    });
    sut.vm.email = 'valid@email.com';
    sut.vm.code = '1234';
    sut.vm.$v.newPassword.$model = '123456';
    sut.vm.$v.confirmPassword.$model = '123456';
    sut.find('form').trigger('submit');
    await localVue.nextTick(); // await resetPassword
    await localVue.nextTick(); // await parseSignupError
    // sut.vm.$forceUpdate();
    expect(
      sut
        .find('.reset-password-page .error')
        .text()
        .toLowerCase(),
    ).toContain('no longer valid');
  });

  it('shows a server invalid password', async () => {
    authService.resetPassword = jest
      .fn()
      .mockImplementation(() => Promise.reject(invalidParamError));
    const sut = shallowMount(ResetPassword, {
      localVue,
      propsData: {
        authService,
      },
    });
    sut.vm.email = 'valid@email.com';
    sut.vm.code = '1234';
    sut.vm.$v.newPassword.$model = '123456';
    sut.vm.$v.confirmPassword.$model = '123456';
    sut.find('form').trigger('submit');
    await localVue.nextTick();
    await localVue.nextTick();
    sut.vm.$forceUpdate();
    expect(
      sut
        .find('.input-error-list')
        .text()
        .toLowerCase(),
    ).toContain('server was unable to validate');
  });

  it('shows a resource error', async () => {
    authService.resetPassword = jest
      .fn()
      .mockImplementation(() => Promise.reject(resourceError));
    const sut = shallowMount(ResetPassword, {
      localVue,
      propsData: {
        authService,
      },
    });
    sut.vm.email = 'valid@email.com';
    sut.vm.code = '1234';
    sut.vm.$v.newPassword.$model = '123456';
    sut.vm.$v.confirmPassword.$model = '123456';
    sut.find('form').trigger('submit');
    await localVue.nextTick();
    await localVue.nextTick();
    sut.vm.$forceUpdate();
    expect(
      sut
        .find('.reset-password-page .error')
        .text()
        .toLowerCase(),
    ).toContain('[config]');
  });
});

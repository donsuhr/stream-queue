import { shallowMount, createLocalVue } from '@vue/test-utils';
import Router from 'vue-router';
import ChangePassword from '../ChangePassword';
import invalidParamError from '../../service/fixtures/change-password-invalid-param.json';
import wrongPasswordError from '../../service/fixtures/change-password-wrong-password.json';

import * as authService from '../../service/auth-service';

let localVue;

describe('Change Password Page', () => {
  beforeEach(() => {
    localVue = createLocalVue();
    localVue.use(Router);
    authService.confirmCode = jest
      .fn()
      .mockImplementation(() => Promise.reject(invalidParamError));
  });

  it('renders', () => {
    const sut = shallowMount(ChangePassword, {
      localVue,
      propsData: {
        authService,
      },
    });
    expect(sut.find('.change-password-page h1').text()).toContain(
      'Change Password',
    );
  });

  it('shows wrong password error', async () => {
    authService.changePassword = jest
      .fn()
      .mockImplementation(() => Promise.reject(wrongPasswordError));
    const sut = shallowMount(ChangePassword, {
      localVue,
      propsData: {
        authService,
      },
    });
    sut.vm.$v.password.$model = '123456';
    sut.vm.$v.newPassword.$model = '123456';
    sut.vm.$v.confirmPassword.$model = '123456';
    sut.find('form').trigger('submit');
    await localVue.nextTick();
    await localVue.nextTick();
    sut.vm.$forceUpdate();
    expect(sut.find('.change-password-page .error p').text()).toContain(
      'Current Password',
    );
  });

  it('it shows a server error for invalid input', async () => {
    authService.changePassword = jest
      .fn()
      .mockImplementation(() => Promise.reject(invalidParamError));
    const sut = shallowMount(ChangePassword, {
      localVue,
      propsData: {
        authService,
      },
    });
    sut.vm.$v.password.$model = '123456';
    sut.vm.$v.newPassword.$model = '123456';
    sut.vm.$v.confirmPassword.$model = '123456';
    sut.find('form').trigger('submit');
    await localVue.nextTick();
    await localVue.nextTick();
    sut.vm.$forceUpdate();

    expect(
      sut
        .findAll('#password + .input-error-list li')
        .at(0)
        .text()
        .toLowerCase(),
    ).toContain('server');
  });

  it('shows a success message on success', async () => {
    authService.changePassword = jest
      .fn()
      .mockImplementation(() => Promise.resolve({}));
    const sut = shallowMount(ChangePassword, {
      localVue,
      propsData: {
        authService,
      },
    });
    sut.vm.$v.password.$model = '123456';
    sut.vm.$v.newPassword.$model = '123456';
    sut.vm.$v.confirmPassword.$model = '123456';
    sut.find('form').trigger('submit');
    await localVue.nextTick();
    await localVue.nextTick();
    sut.vm.$forceUpdate();
    expect(sut.find('.change-password-page').text()).toContain(
      'Password change successful',
    );
  });
});

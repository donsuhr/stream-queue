import { shallowMount, createLocalVue } from '@vue/test-utils';
import Router from 'vue-router';
import ResendCode from '../ResendCode';
import invalidError from '../../service/fixtures/resend-code-invalid-param.json';
import resourceError from '../../service/fixtures/resend-code-resource-not-found.json';
import resendCodeSuccess from '../../service/fixtures/resend-code-success.json';
import userNotFoundError from '../../service/fixtures/resend-code-not-found.json';
import { LS_KEY_SIGNUP_EMAIL } from '../../service/auth-service-const';

import * as authService from '../../service/auth-service';

let localVue;

describe('Resend Code Page', () => {
  beforeEach(() => {
    localVue = createLocalVue();
    localVue.use(Router);

    authService.resendCode = jest
      .fn()
      .mockImplementation(() => Promise.reject(invalidError));
  });

  it('renders', () => {
    const sut = shallowMount(ResendCode, {
      localVue,
      propsData: {
        authService,
      },
    });
    expect(sut.find('.resend-code-page h1').text()).toContain(
      'Resend Code',
    );
  });

  it('it calls though to resend', async () => {
    const sut = shallowMount(ResendCode, {
      localVue,
      propsData: {
        authService,
      },
    });

    sut.vm.$v.email.$model = 'valid@email.com';
    sut.find('form').trigger('submit');
    expect(authService.resendCode.mock.calls).toHaveLength(1);
  });

  it('it shows error for invalid input', async () => {
    const sut = shallowMount(ResendCode, {
      localVue,
      propsData: {
        authService,
      },
    });
    sut.vm.$v.email.$model = 'valid@email.com';
    sut.find('form').trigger('submit');
    await localVue.nextTick();
    await localVue.nextTick();
    expect(
      sut
        .find('.resend-code-page .error p')
        .text()
        .toLowerCase(),
    ).toContain('invalid');
  });

  it('it shows a config error', async () => {
    authService.resendCode = jest
      .fn()
      .mockImplementation(() => Promise.reject(resourceError));

    const sut = shallowMount(ResendCode, {
      localVue,
      propsData: {
        authService,
      },
    });
    sut.vm.$v.email.$model = 'valid@email.com';
    sut.find('form').trigger('submit');
    await localVue.nextTick();
    await localVue.nextTick();
    expect(
      sut
        .find('.resend-code-page .error p')
        .text()
        .toLowerCase(),
    ).toContain('config');
  });

  it('it shows a server error for invalid input', async () => {
    const sut = shallowMount(ResendCode, {
      localVue,
      propsData: {
        authService,
      },
    });
    sut.vm.$v.email.$model = 'valid@email.com';
    sut.find('form').trigger('submit');
    await localVue.nextTick();
    await localVue.nextTick();
    sut.vm.$forceUpdate();

    expect(
      sut
        .findAll('#email + .input-error-list li')
        .at(0)
        .text()
        .toLowerCase(),
    ).toContain('server');
  });

  it('shows an error for no user found', async () => {
    authService.resendCode = jest
      .fn()
      .mockImplementation(() => Promise.reject(userNotFoundError));
    const sut = shallowMount(ResendCode, {
      localVue,
      propsData: {
        authService,
      },
    });
    sut.vm.$v.email.$model = 'not@found.com';
    sut.find('form').trigger('submit');
    await localVue.nextTick();
    await localVue.nextTick();
    sut.vm.$forceUpdate();
    expect(
      sut
        .find('.resend-code-page .error p')
        .text()
        .toLowerCase(),
    ).toContain('not found');
  });

  it('shows client side validation error', async () => {
    const sut = shallowMount(ResendCode, {
      localVue,
      propsData: {
        authService,
      },
    });
    sut.vm.$v.email.$model = 'invalid';
    sut.find('form').trigger('submit');
    await localVue.nextTick();
    await localVue.nextTick();
    sut.vm.$forceUpdate();
    expect(
      sut
        .findAll('#email + .input-error-list li')
        .at(0)
        .text()
        .toLowerCase(),
    ).toContain('enter a valid email');
  });

  it('saves the email used to sign up', async () => {
    authService.resendCode = jest
      .fn()
      .mockImplementation(() => Promise.resolve(resendCodeSuccess));
    const sut = shallowMount(ResendCode, {
      localVue,
      propsData: {
        authService,
      },
    });
    const testEmail = 'valid@email.com';
    sut.vm.$v.email.$model = testEmail;
    sut.find('form').trigger('submit');
    await localVue.nextTick();
    await localVue.nextTick();
    sut.vm.$forceUpdate();

    expect(localStorage.setItem).toHaveBeenLastCalledWith(
      LS_KEY_SIGNUP_EMAIL,
      testEmail,
    );
  });

  it('redirects to confirm code after success', async () => {
    authService.resendCode = jest
      .fn()
      .mockImplementation(() => Promise.resolve(resendCodeSuccess));
    const router = new Router();
    router.push = jest.fn();
    const sut = shallowMount(ResendCode, {
      localVue,
      router,
      propsData: {
        authService,
      },
    });
    sut.vm.$v.email.$model = 'valid@email.com';
    sut.find('form').trigger('submit');
    await localVue.nextTick();
    await localVue.nextTick();
    sut.vm.$forceUpdate();
    expect(sut.vm.$router.push).toHaveBeenCalledWith('confirm');
  });

  it('loads email from localstorage', () => {
    const testEmail = 'valid@email.com';
    localStorage.setItem(LS_KEY_SIGNUP_EMAIL, testEmail);
    const sut = shallowMount(ResendCode, {
      localVue,
      propsData: {
        authService,
      },
    });
    expect(sut.find('#email').element.value).toBe(testEmail);
  });
});

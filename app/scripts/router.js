import Vue from 'vue';
import Router from 'vue-router';
import Movies from './pages/Movies';

Vue.use(Router);

const Import = () => import(/* webpackChunkName: "import-page" */ './pages/Import');
const SignUp = () => import(/* webpackChunkName: "auth-pages" */ './pages/SignUp');
const Login = () => import(/* webpackChunkName: "login-page" */ './pages/Login');
const ResendCode = () => import(/* webpackChunkName: "auth-pages" */ './pages/ResendCode');
const ConfirmCode = () => import(/* webpackChunkName: "auth-pages" */ './pages/ConfirmCode');
const ChangePassword = () => import(/* webpackChunkName: "auth-pages" */ './pages/ChangePassword');
const ForgotPassword = () => import(/* webpackChunkName: "auth-pages" */ './pages/ForgotPassword');
const ResetPassword = () => import(/* webpackChunkName: "auth-pages" */ './pages/ResetPassword');

export default function createRouter({ domDataAttributes, authService } = {}) {
  const router = new Router({
    // mode: process.env.NODE_ENV === 'production' ? 'history' : 'hash',
    routes: [
      {
        path: '/queue/:filter?',
        name: 'queue',
        component: Movies,
      },
      {
        path: '/import',
        name: 'import',
        component: Import,
      },
      {
        path: '/signup',
        name: 'signup',
        component: SignUp,
        props: {
          authService,
        },
      },
      {
        path: '/confirm',
        name: 'confirm',
        component: ConfirmCode,
        props: {
          authService,
        },
      },
      {
        path: '/resend-code',
        name: 'resendCode',
        component: ResendCode,
        props: {
          authService,
        },
      },
      {
        path: '/change-password',
        name: 'changePassword',
        component: ChangePassword,
        props: {
          authService,
        },
      },
      {
        path: '/forgot-password',
        name: 'forgotPassword',
        component: ForgotPassword,
        props: {
          authService,
        },
      },
      {
        path: '/reset-password',
        name: 'resetPassword',
        component: ResetPassword,
        props: {
          authService,
        },
      },
      {
        path: '/login',
        name: 'login',
        component: Login,
        props: {
          authService,
        },
      },
      {
        path: '/logout',
        name: 'logout',
        beforeEnter(to, from, next) {
          authService.logout();
          next({ name: 'login' });
        },
      },
    ],
    base: domDataAttributes?.base ?? '/',
  });
  router.beforeEach((to, from, next) => {
    if (to.path === '/') {
      if (!to.params.filter) {
        return next({ name: 'queue', params: { filter: 'canstream' } });
      }
    }
    if (to.path === '/queue') {
      if (!to.params.filter) {
        return next({ name: 'queue', params: { filter: 'canstream' } });
      }
    }
    return next();
  });
  return router;
}

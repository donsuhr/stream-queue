import Vue from 'vue';
import Vuex from 'vuex';
import { createParsedMovieStore } from './components/import/parsed-movie-store';
import { createQueueStore } from './components/queue/queue-store';
import { createProviderToggleStore } from './components/queue/provider-toggle-store';
import { createAuthenticatedStore } from './authenticated-store';
import { store as uiStore } from './ui-store';
import { AUTH_CHANGE } from './service/auth-service-const';

Vue.use(Vuex);

export default function createStore({
  domDataAttributes,
  router,
  authService,
  queueService,
}) {
  const parsedMovieStore = createParsedMovieStore({
    router,
    authService,
    queueService,
  });
  const queueStore = createQueueStore({
    authService,
    queueService,
  });
  const providerToggleStore = createProviderToggleStore();
  const authenticatedStore = createAuthenticatedStore({ authService });

  const store = new Vuex.Store({
    state: {
      domDataAttributes,
    },
    modules: {
      parsedMovies: parsedMovieStore,
      queue: queueStore,
      providerToggle: providerToggleStore,
      authenticated: authenticatedStore,
      ui: uiStore,
    },
  });

  const authEvents = authService.emitter;
  let currentAuthState = false;
  authEvents.on(AUTH_CHANGE, (isAuth) => {
    store.dispatch('authenticated/setAuth', isAuth);
    if (!isAuth) {
      store.dispatch('queue/logout');
    }
    if (isAuth !== currentAuthState && isAuth) {
      store.dispatch('queue/login');
    }
    currentAuthState = isAuth;
  });
  store.dispatch('authenticated/checkAuth');

  return store;
}

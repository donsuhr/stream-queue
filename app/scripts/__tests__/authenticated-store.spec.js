/* eslint-env jest */

import { createAuthenticatedStore } from '../authenticated-store';
import authService from '../service/__mocks__/auth-service';
import { LS_KEY_EVER_AUTH } from '../service/auth-service-const';

let sut;

describe('queue-store', () => {
  beforeEach(() => {
    sut = createAuthenticatedStore({ authService });
  });

  describe('mutations', () => {
    it('setAuthenticated', () => {
      expect(sut.state.isAuthenticated).toEqual(null);
      sut.mutations.setAuthenticated(sut.state, true);
      expect(sut.state.isAuthenticated).toEqual(true);
    });

    it('setLoading', () => {
      expect(sut.state.loading).toBe(false);
      sut.mutations.setLoading(sut.state, true);
      expect(sut.state.loading).toBe(true);
    });
  });

  describe('getters', () => {
    it('loading', () => {
      const state = {
        loading: false,
      };
      expect(sut.getters.loading(state)).toBe(state.loading);
    });

    it('isAuthenticated', () => {
      const state = { isAuthenticated: true };
      expect(sut.getters.isAuthenticated(state)).toEqual(
        state.isAuthenticated,
      );
    });
  });

  describe('actions', () => {
    it('checkAuth - has cookie', async () => {
      localStorage.setItem(LS_KEY_EVER_AUTH, true);
      const commit = jest.fn();
      const state = {
        isAuthenticated: null,
        loading: false,
      };
      const rootState = {};
      authService.loadIsAuthenticated = jest
        .fn()
        .mockImplementation(() => Promise.resolve(true));
      await sut.actions.checkAuth({ commit, state, rootState });
      expect(commit.mock.calls[0][0]).toBe('setLoading');
      expect(commit.mock.calls[0][1]).toBe(true);
      expect(authService.loadIsAuthenticated).toHaveBeenCalled();
      expect(commit.mock.calls[1][0]).toBe('setAuthenticated');
      expect(commit.mock.calls[1][1]).toBe(true);
    });

    it('checkAuth - no cookie', () => {
      localStorage.setItem(LS_KEY_EVER_AUTH, false);
      const commit = jest.fn();
      const state = {
        isAuthenticated: null,
        loading: false,
      };
      const rootState = {};
      sut.actions.checkAuth({ commit, state, rootState });
      expect(commit.mock.calls[1][0]).toBe('setAuthenticated');
      expect(commit.mock.calls[1][1]).toBe(false);
    });

    it('setAuth', () => {
      const commit = jest.fn();
      const state = {};
      const rootState = {};
      sut.actions.setAuth({ commit, state, rootState }, true);
      expect(commit.mock.calls[0][0]).toBe('setAuthenticated');
      expect(commit.mock.calls[0][1]).toBe(true);
      sut.actions.setAuth({ commit, state, rootState }, false);
      expect(commit.mock.calls[1][0]).toBe('setAuthenticated');
      expect(commit.mock.calls[1][1]).toBe(false);
    });
  });
});

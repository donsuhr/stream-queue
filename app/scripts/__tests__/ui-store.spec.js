/* eslint-env jest */

import { store } from '../ui-store';

const sut = store;

describe('store', () => {
  beforeEach(() => {});

  afterEach(() => {});

  describe('mutations', () => {
    it('isGridView', () => {
      expect(sut.state.isGridView).toEqual(false);
      sut.mutations.isGridView(sut.state, true);
      expect(sut.state.isGridView).toEqual(true);
    });
  });

  describe('getters', () => {
    it('isGridView', () => {
      const state = {
        isGridView: false,
      };
      expect(sut.getters.isGridView(state)).toBe(state.isGridView);
    });
  });

  describe('actions', () => {
    it('toggleGridView', async () => {
      const commit = jest.fn();
      const state = {
        isGridView: false,
      };
      const rootState = {};
      sut.actions.toggleGridView({ commit, state, rootState });
      expect(commit.mock.calls[0][0]).toBe('isGridView');
      expect(commit.mock.calls[0][1]).toBe(true);
    });
  });
});

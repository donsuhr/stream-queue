/* eslint-env jest */

import createStore from '../store';

const domDataAttributes = {};
const router = {};
const authService = { emitter: { on: jest.fn() } };
const queueService = {};

describe('store', () => {
  beforeEach(() => {});

  afterEach(() => {});

  it('creates', () => {
    expect(() => createStore({
      domDataAttributes,
      router,
      authService,
      queueService,
    })).not.toThrow();
  });
});

/* globals serviceWorkerVersion, process */
// import '@babel/polyfill';
import runtime from 'serviceworker-webpack-plugin/lib/runtime';
import Vue from 'vue';
import { parseJSON } from 'fetch-json-helpers';
import createStore from './store';
import createRouter from './router';
import App from './App';
import * as authService from './service/auth-service-facade';
import DynamoDB from './service/dynamodb-facade';
import { getDomDataAttributes } from './util/domAttributes-util';
import '../styles/main.scss';
import { create as createQueueService } from './service/queue-service';
import lozadDirective from './directives/lozad';

const queueService = createQueueService({ authService, DynamoDB });

const el = '#app';

const domDataAttributes = getDomDataAttributes(document.querySelector(el));
const router = createRouter({ domDataAttributes, authService });
const store = createStore({
  domDataAttributes,
  router,
  authService,
  queueService,
});

// eslint-disable-next-line  no-new
new Vue({
  el,
  store,
  router,
  render: (h) => h(App, {}),
});

Vue.directive('lozad', lozadDirective);

if ('serviceWorker' in navigator) {
  // navigator.serviceWorker.register('/sw.js');

  // use if using a refresh button
  // const updateReady = (worker) => {
  //     window.foo = () => {
  //         worker.postMessage({ action: 'skipWaiting' });
  //     };
  // };
  //
  // const installing = (worker) => {
  //     worker.addEventListener('statechange', () => {
  //         if (worker.state === 'installed') {
  //             updateReady(worker);
  //         }
  //     });
  // };

  runtime.register({
    scope: process.env.NODE_ENV === 'production' ? '/projects/queue/' : '/',
  });
  // use if using a refresh button
  // .then((reg) => {
  //     if (reg.waiting) {
  //         updateReady(reg.waiting);
  //         return;
  //     }
  //
  //     if (reg.installing) {
  //         installing(reg.installing);
  //         return;
  //     }
  //
  //     reg.addEventListener('updatefound', () => {
  //         installing(reg.installing);
  //     });
  // });

  navigator.serviceWorker.onmessage = (evt) => {
    const message = JSON.parse(evt.data);
    const isRefresh = message.type === 'refresh';
    const isQueue = !!message.url.match(/\/queue$/i);
    if (isRefresh && isQueue) {
      caches
        .open(serviceWorkerVersion)
        .then((cache) => cache.match(message.url))
        .then(parseJSON)
        .then((data) => {
          store.commit('queue/bgUpdateItems', data);
        });
    }
  };
  // use if using a refresh button. I'm just doing it in the bg on 'install'
  // let refreshing = false;
  // navigator.serviceWorker.addEventListener('controllerchange', () => {
  //     if (refreshing) return;
  //     window.location.reload();
  //     refreshing = true;
  // });
}

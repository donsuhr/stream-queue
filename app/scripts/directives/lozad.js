import lozad from 'lozad';

const lozadDirective = {
  bind(el, binding) {
    el.setAttribute('data-src', binding.value);
    const observer = lozad(el);
    observer.observe();
  },
  update(el, binding) {
    if (binding.oldValue !== binding.value) {
      el.setAttribute('data-src', binding.value);
      if (el.getAttribute('data-loaded') === 'true') {
        el.setAttribute('src', binding.value);
      }
    }
  },
};

export default lozadDirective;

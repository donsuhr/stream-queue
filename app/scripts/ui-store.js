const store = {
  namespaced: true,
  state: {
    isGridView: false,
  },
  mutations: {
    isGridView(state, data) {
      state.isGridView = data;
    },
  },
  actions: {
    toggleGridView({ commit, state, rootState }) {
      commit('isGridView', !state.isGridView);
    },
  },
  getters: {
    isGridView: (state) => state.isGridView,
  },
};

export { store };

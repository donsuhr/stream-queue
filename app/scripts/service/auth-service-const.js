export const LS_KEY_SIGNUP_EMAIL = 'signupEmail';
export const AUTH_CHANGE = 'AUTH_CHANGE';
export const LS_KEY_EVER_AUTH = 'hasEverSignedIn';
export const LS_KEY_IDENTITY_ID = 'identityId';

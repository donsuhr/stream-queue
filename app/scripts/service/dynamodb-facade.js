let DynamoDB;

function load() {
  if (!DynamoDB) {
    // prettier-ignore
    return import(/* webpackChunkName: "authUtils" */ 'aws-sdk/clients/dynamodb')
      .then((module) => {
        DynamoDB = module;
      });
  }
  return Promise.resolve();
}

function DocumentClient(...args) {
  return (async () => {
    await load();
    return new DynamoDB.DocumentClient(...args);
  })();
}

export default { DocumentClient };

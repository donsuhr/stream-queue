import EventEmitter from 'events'; // eslint-disable-line import/no-extraneous-dependencies

let authUtils;

class MyEmitter extends EventEmitter {
}

export const emitter = new MyEmitter();

function load() {
  if (load.cached) {
    return load.cached;
  }
  load.cached = import(/* webpackChunkName: "authUtils" */ './auth-service').then(
    (module) => {
      load.cached = Promise.resolve();
      authUtils = module;
      authUtils.emitter.emit = (...args) => {
        emitter.emit(...args);
      };
    },
  );
  return load.cached;
}

export async function getCurrentSession(...args) {
  await load();
  return authUtils.getCurrentSession(...args);
}

export async function loadIdentityId(...args) {
  await load();
  return authUtils.loadIdentityId(...args);
}

export async function loadIdtoken(...args) {
  await load();
  return authUtils.loadIdtoken(...args);
}

export async function getUserAttributes(...args) {
  await load();
  return authUtils.getUserAttributes(...args);
}

export async function signUp(...args) {
  await load();
  return authUtils.signUp(...args);
}

export async function confirmCode(...args) {
  await load();
  return authUtils.confirmCode(...args);
}

export async function forgotPassword(...args) {
  await load();
  return authUtils.forgotPassword(...args);
}

export async function resetPassword(...args) {
  await load();
  return authUtils.resetPassword(...args);
}

export async function changePassword(...args) {
  await load();
  return authUtils.changePassword(...args);
}

export async function logout(...args) {
  await load();
  return authUtils.logout(...args);
}

export async function resendCode(...args) {
  await load();
  return authUtils.resendCode(...args);
}

export async function login(...args) {
  await load();
  return authUtils.login(...args);
}

export async function parseSignupError(...args) {
  await load();
  return authUtils.parseSignupError(...args);
}

export async function loadIsAuthenticated(...args) {
  await load();
  return authUtils.loadIsAuthenticated(...args);
}

/* eslint-env jest */

import {
  parseSignupError,
  getCurrentSession,
  loadIdentityId,
} from '../auth-service';
import invalidError from '../fixtures/sign-up-invalid-param.json';

describe('auth-service', () => {
  test('parseSignupError', () => {
    const result = parseSignupError(invalidError);
    expect(result.message).toContain('6');
    expect(result.errors.length).toBe(6);
  });

  test('getCurrentSession rejects', () => {
    expect.assertions(1);
    return expect(getCurrentSession(true)).rejects.toThrowError(
      'no current session exists',
    );
  });

  test('loadIdentityId rejects', () => {
    expect.assertions(1);
    return expect(loadIdentityId()).rejects.toThrowError(
      'no current session exists',
    );
  });
});

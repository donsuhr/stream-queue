import nock from 'nock';
import { create } from '../queue-service';
import config from '../../config';
import authService, { identityId } from '../__mocks__/auth-service';
import { LS_KEY_EVER_AUTH } from '../auth-service-const';

let sut;
let currentUpdateMock;
let currentGetMock;
let currentGetResult;
let currentUpdateResult;

const DynamoDB = {
  // eslint-disable-next-line object-shorthand
  DocumentClient: function DocumentClient() {
    this.query = currentGetMock;
    this.batchWrite = currentUpdateMock;
  },
};

function createDynamoDB() {
  currentUpdateMock = jest.fn().mockImplementation(() => ({
    promise: () => Promise.resolve(currentUpdateResult),
  }));
  currentGetMock = jest.fn().mockImplementation(() => ({
    promise: () => Promise.resolve(currentGetResult),
  }));
}

describe('queue service', () => {
  beforeEach(() => {
    nock.disableNetConnect();
    createDynamoDB();
    currentGetResult = {
      Items: [],
    };
    currentUpdateResult = {};
    localStorage.setItem(LS_KEY_EVER_AUTH, true);
    sut = create({ authService, DynamoDB });
  });

  describe('getQueue', () => {
    it('can mock', async () => {
      await sut.getQueue();
      expect(currentGetMock).toHaveBeenCalled();
      expect(
        currentGetMock.mock.calls[0][0].ExpressionAttributeValues,
      ).toEqual({ ':identityId': identityId });
    });

    it('calls out for unauthenticated request', async () => {
      localStorage.setItem(LS_KEY_EVER_AUTH, false);
      const scope = nock(`${config.AWS.apiGatewayUrl}/`)
        .get('/queue')
        .reply(200, {});
      await sut.getQueue();
      expect(scope.isDone()).toBe(true);
    });

    it('returns default queue when user queue throws', async () => {
      localStorage.setItem(LS_KEY_EVER_AUTH, true);

      const localAuthService = {
        loadIdentityId() {
          return Promise.reject();
        },
      };
      sut = create({ authService: localAuthService, DynamoDB });
      const scope = nock(`${config.AWS.apiGatewayUrl}/`)
        .get('/queue')
        .reply(200, {});
      // eslint-disable-next-line no-console
      console.log = jest.fn();
      await sut.getQueue();
      expect(scope.isDone()).toBe(true);
      // eslint-disable-next-line no-console
      expect(console.log).toHaveBeenCalled();
    });
  });

  describe('patchQueue', () => {
    it('sets the key', async () => {
      await sut.patchQueue();
      expect(
        currentGetMock.mock.calls[0][0].ExpressionAttributeValues,
      ).toEqual({ ':identityId': identityId });
    });

    it('has PutRequests when adding', async () => {
      currentGetResult = { Items: [] };
      await sut.patchQueue([{ netflixId: 1 }]);
      expect(
        currentUpdateMock.mock.calls[0][0].RequestItems[
          config.AWS.queueTable
        ][0].PutRequest.Item.netflixId,
      ).toBe(1);
    });

    it('has DeleteRequest for removes', async () => {
      currentGetResult = {
        Items: [{ netflixId: 1 }],
      };
      await sut.patchQueue([{ netflixId: 2 }]);
      // first call is remove
      expect(
        currentUpdateMock.mock.calls[0][0].RequestItems[
          config.AWS.queueTable
        ][0].DeleteRequest.Key.netflixId,
      ).toBe(1);
      expect(
        currentUpdateMock.mock.calls[0][0].RequestItems[
          config.AWS.queueTable
        ][1].PutRequest.Item.netflixId,
      ).toBe(2);
    });

    it('sets the expression to remove the correct indexes', async () => {
      const current = [
        { netflixId: 1 },
        { netflixId: 2 },
        { netflixId: 3 },
        { netflixId: 4 },
      ];
      const future = [{ netflixId: 2 }, { netflixId: 4 }];
      currentGetResult = {
        Items: current,
      };
      await sut.patchQueue(future);
      expect(
        currentUpdateMock.mock.calls[0][0].RequestItems[
          config.AWS.queueTable
        ][0].DeleteRequest.Key.netflixId,
      ).toBe(1);
      expect(
        currentUpdateMock.mock.calls[0][0].RequestItems[
          config.AWS.queueTable
        ][1].DeleteRequest.Key.netflixId,
      ).toBe(3);
    });

    it('sets the values to add the new items', async () => {
      const future = [{ netflixId: 2 }, { netflixId: 4 }];
      await sut.patchQueue(future);

      function match(adds) {
        return (
          adds[0].PutRequest.Item.netflixId === 2
          && adds[1].PutRequest.Item.netflixId === 4
        );
      }

      expect(
        match(
          currentUpdateMock.mock.calls[0][0].RequestItems[
            config.AWS.queueTable
          ],
        ),
      ).toBe(true);
    });

    it('adds a time to the adds', async () => {
      const future = [{ netflixId: 2 }, { netflixId: 4 }];
      await sut.patchQueue(future);

      function match(adds) {
        return (
          adds[0].PutRequest.Item.netflixId === 2
          && Object.hasOwnProperty.call(adds[0].PutRequest.Item, 'createdAt')
          && Object.hasOwnProperty.call(adds[0].PutRequest.Item, 'updatedAt')
        );
      }

      expect(
        match(
          currentUpdateMock.mock.calls[0][0].RequestItems[
            config.AWS.queueTable
          ],
        ),
      ).toBe(true);
    });
  });
});

import config from 'config';
import { checkStatus, parseJSON } from 'fetch-json-helpers';
import { LS_KEY_EVER_AUTH } from './auth-service-const';

export function create({ authService, DynamoDB }) {
  function getDefaultQueue() {
    return fetch(`${config.AWS.apiGatewayUrl}/queue`)
      .then(checkStatus)
      .then(parseJSON);
  }

  async function getQueue() {
    const hasEverLoggedIn = localStorage.getItem(LS_KEY_EVER_AUTH);
    if (hasEverLoggedIn === 'true') {
      try {
        const identityId = await authService.loadIdentityId();
        const docClient = await new DynamoDB.DocumentClient({
          apiVersion: '2012-08-10',
        });
        return docClient
          .query({
            TableName: config.AWS.queueTable,
            KeyConditionExpression: 'identityId = :identityId',
            ExpressionAttributeValues: {
              ':identityId': identityId,
            },
          })
          .promise()
          .then((result) => result.Items);
      } catch (e) {
        // eslint-disable-next-line no-console
        console.log('caught error', e);
        return getDefaultQueue();
      }
    }

    return getDefaultQueue();
  }

  async function patchQueue(items = []) {
    let queue = [];
    try {
      queue = await getQueue();
    } catch (e) {
      // no queue or no queueItems property
    }

    const identityId = await authService.loadIdentityId();
    const adds = items
      .filter((x) => !queue.some((y) => y.netflixId === x.netflixId))
      .map((x) => ({
        PutRequest: {
          Item: {
            ...x,
            identityId,
            createdAt: new Date().toISOString(),
            updatedAt: new Date().toISOString(),
          },
        },
      }));

    const removes = queue
      .filter((x) => !items.some((y) => y.netflixId === x.netflixId))
      .map((x) => ({
        DeleteRequest: {
          Key: { identityId, netflixId: x.netflixId },
        },
      }));

    const chunks = [];
    const actions = [...removes, ...adds];
    while (actions.length) {
      chunks[chunks.length] = actions.splice(0, 25);
    }

    const docClient = await new DynamoDB.DocumentClient({
      apiVersion: '2012-08-10',
    });
    const requests = chunks.map((x) => docClient
      .batchWrite({
        RequestItems: {
          [config.AWS.queueTable]: x,
        },
      })
      .promise());
    return Promise.all(requests);
  }

  return {
    getQueue,
    patchQueue,
  };
}

export const identityId = 'us-west-2:123';

const authService = {
  emitter: {
    on() {},
  },
  loadIsAuthenticated() {
    return Promise.resolve();
  },
  loadIdentityId() {
    return identityId;
  },
  loadIdtoken() {
    return Promise.resolve('token123');
  },
};

export default authService;

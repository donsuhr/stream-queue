// https://docs.aws.amazon.com/cognito/latest/developerguide/using-amazon-cognito-user-identity-pools-javascript-examples.html
import config from 'config';
import {
  CognitoUserPool,
  CognitoUserAttribute,
  CognitoUser,
  AuthenticationDetails,
} from 'amazon-cognito-identity-js';
import AWS from 'aws-sdk/lib/core';
import 'aws-sdk/lib/credentials/cognito_identity_credentials';
import EventEmitter from 'events'; // eslint-disable-line import/no-extraneous-dependencies
import {
  LS_KEY_EVER_AUTH,
  AUTH_CHANGE,
  LS_KEY_IDENTITY_ID,
} from './auth-service-const';

class MyEmitter extends EventEmitter {}

export const emitter = new MyEmitter();

/* istanbul ignore next */
function cognitoUserPool() {
  return new CognitoUserPool({
    UserPoolId: config.AWS.USER_POOL_ID,
    ClientId: config.AWS.CLIENT_ID,
  });
}

function getCurrentUser() {
  const userPool = cognitoUserPool();
  return userPool.getCurrentUser();
}

function isCachedSessionExpired() {
  // eslint-disable-next-line no-use-before-define
  return !!getCurrentSession.session && !getCurrentSession.session.isValid();
}

function onSessionAcquired(cognitoUserSession) {
  localStorage.setItem(LS_KEY_EVER_AUTH, true);
  const token = cognitoUserSession.getIdToken().getJwtToken();

  const credentials = new AWS.CognitoIdentityCredentials({
    IdentityPoolId: config.AWS.IDENTITY_POOL_ID,
    Logins: {
      [`cognito-idp.${config.AWS.AWS_REGION}.amazonaws.com/${config.AWS.USER_POOL_ID}`]: token,
    },
  });
  AWS.config.update({ region: config.AWS.AWS_REGION, credentials });
  emitter.emit(AUTH_CHANGE, true);
}

export function getCurrentSession(forceReload = false) {
  if (forceReload) {
    getCurrentSession.cached = null;
    getCurrentSession.session = null;
  }
  if (getCurrentSession.cached && !isCachedSessionExpired()) {
    return getCurrentSession.cached;
  }
  getCurrentSession.cached = new Promise((resolve, reject) => {
    const cognitoUser = getCurrentUser();
    if (cognitoUser != null) {
      cognitoUser.getSession((err, cognitoUserSession) => {
        if (err) {
          reject(err);
        } else {
          getCurrentSession.session = cognitoUserSession;
          onSessionAcquired(cognitoUserSession);
          resolve({ cognitoUser, cognitoUserSession });
        }
      });
    } else {
      emitter.emit(AUTH_CHANGE, false);
      reject(new Error('no current session exists'));
    }
  });
  return getCurrentSession.cached;
}

export function loadIdentityId() {
  return new Promise((resolve, reject) => {
    getCurrentSession()
      .then(() => {
        const lsid = localStorage.getItem(LS_KEY_IDENTITY_ID);
        if (lsid) {
          resolve(lsid);
        } else {
          AWS.config.credentials.get((err) => {
            if (err) {
              // eslint-disable-next-line no-console
              console.error(err);
              reject(err);
            } else {
              localStorage.setItem(
                LS_KEY_IDENTITY_ID,
                AWS.config.credentials.identityId,
              );
              resolve(AWS.config.credentials.identityId);
            }
          });
        }
      })
      .catch((e) => {
        reject(e);
      });
  });
}

export function loadIdtoken() {
  return new Promise((resolve, reject) => {
    getCurrentSession().then(({ cognitoUserSession }) => {
      resolve(cognitoUserSession.getIdToken().getJwtToken());
    });
  });
}

export function getUserAttributes() {
  return new Promise((resolve, reject) => {
    getCurrentSession()
      .then(({ cognitoUser }) => {
        cognitoUser.getUserAttributes((error, result) => {
          if (error) {
            // eslint-disable-next-line no-console
            console.error(error);
            reject(error);
          }
          const ret = result.reduce((acc, x) => {
            acc[x.getName()] = x.getValue();
            return acc;
          }, {});
          resolve(ret);
        });
      })
      .catch((err) => {
        reject(err);
      });
  });
}

export function signUp(email, pass) {
  const userPool = cognitoUserPool();
  const attributeList = [
    new CognitoUserAttribute({ Name: 'email', Value: email }),
  ];
  // eslint-disable-next-line max-len
  return new Promise((resolve, reject) => userPool.signUp(email, pass, attributeList, null, (err, result) => {
    if (err) {
      // eslint-disable-next-line no-console
      console.error(err);
      reject(err);
    } else {
      resolve(result);
    }
  }));
}

export function confirmCode(email, code) {
  const userPool = cognitoUserPool();

  const cognitoUser = new CognitoUser({
    Username: email,
    Pool: userPool,
  });

  return new Promise((resolve, reject) => {
    cognitoUser.confirmRegistration(code, true, (err, result) => {
      if (err) {
        // eslint-disable-next-line no-console
        console.error(err);
        reject(err);
      } else {
        resolve(result);
      }
    });
  });
}

export function changePassword(oldPassword, newPassword) {
  return new Promise((resolve, reject) => {
    getCurrentSession().then(({ cognitoUser }) => {
      cognitoUser.changePassword(oldPassword, newPassword, (err, result) => {
        if (err) {
          // eslint-disable-next-line no-console
          console.error(err);
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  });
}

export function forgotPassword(email) {
  const userPool = cognitoUserPool();

  const cognitoUser = new CognitoUser({
    Username: email,
    Pool: userPool,
  });

  return new Promise((resolve, reject) => {
    cognitoUser.forgotPassword({
      onSuccess: (result) => {
        resolve(result);
      },
      onFailure: (err) => {
        // eslint-disable-next-line no-console
        console.error(err);
        reject(err);
      },
    });
  });
}

export function resetPassword(email, code, password) {
  const userPool = cognitoUserPool();

  const cognitoUser = new CognitoUser({
    Username: email,
    Pool: userPool,
  });

  return new Promise((resolve, reject) => {
    cognitoUser.confirmPassword(code, password, {
      onSuccess: (result) => {
        resolve(result);
      },
      onFailure: (err) => {
        // eslint-disable-next-line no-console
        console.error(err);
        reject(err);
      },
    });
  });
}

export const logout = () => {
  getCurrentSession.cached = null;
  getCurrentSession.session = null;
  localStorage.removeItem(LS_KEY_EVER_AUTH);
  localStorage.removeItem(LS_KEY_IDENTITY_ID);
  const currentUser = getCurrentUser();
  if (currentUser !== null) {
    currentUser.signOut();
    AWS.config.credentials.clearCachedId();
    emitter.emit(AUTH_CHANGE, false);
  }
};

export function resendCode(email) {
  const userPool = cognitoUserPool();

  const cognitoUser = new CognitoUser({
    Username: email,
    Pool: userPool,
  });

  return new Promise((resolve, reject) => {
    cognitoUser.resendConfirmationCode((err, result) => {
      if (err) {
        // eslint-disable-next-line no-console
        console.error(err);
        reject(err);
      } else {
        resolve(result);
      }
    });
  });
}

export function login(email, pass) {
  const userPool = cognitoUserPool();
  const user = new CognitoUser({ Username: email, Pool: userPool });
  const authenticationDetails = new AuthenticationDetails({
    Username: email,
    Password: pass,
  });

  return new Promise((resolve, reject) => user.authenticateUser(authenticationDetails, {
    onSuccess: (cognitoUserSession) => {
      const cognitoUser = getCurrentUser();
      onSessionAcquired(cognitoUserSession);
      resolve({ cognitoUser, cognitoUserSession });
    },
    onFailure: (err) => {
      // eslint-disable-next-line no-console
      console.error(err);
      reject(err);
    },
  }));
}

export function parseSignupError(error) {
  const errors = error.message.split(';');
  const message = errors[0].substring(0, errors[0].indexOf(':'));
  errors[0] = errors[0].substring(errors[0].indexOf(': ') + 1);
  return {
    message,
    errors,
    code: error.code,
  };
}

export function loadIsAuthenticated() {
  return new Promise((resolve) => {
    getCurrentSession()
      .then(() => resolve(true))
      .catch(() => resolve(false));
  });
}

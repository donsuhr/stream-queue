import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import ViewToggleBtn from '../view-toggle-btn';

let store;
let localVue;

describe('ViewToggleBtn', () => {
  beforeEach(() => {
    localVue = createLocalVue();
    localVue.use(Vuex);
    store = new Vuex.Store({
      state: {},
    });
  });

  it('renders', () => {
    const sut = shallowMount(ViewToggleBtn, {
      store,
      localVue,
    });
    expect(sut.find('Button').text()).toContain('View Grid');
  });

  it('toggles', async () => {
    const actions = {
      'ui/toggleGridView': jest.fn(),
    };
    store = new Vuex.Store({
      state: {},
      actions,
    });
    const sut = shallowMount(ViewToggleBtn, {
      store,
      localVue,
    });
    sut.find('button').trigger('click');
    await localVue.nextTick();
    expect(actions['ui/toggleGridView'].mock.calls).toHaveLength(1);
  });
});

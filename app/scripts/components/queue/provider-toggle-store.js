import { getProvidersByType } from './providers';

const LOCAL_STORAGE_KEY = 'provider-toggles';

function saveToLc(state) {
  localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(Object.values(state)));
}

function loadAndPopulateDefaultItems() {
  const result = getProvidersByType()
    .map((x) => ({ id: x.id, enabled: true }))
    .reduce((acc, x) => {
      acc[x.id] = x;
      return acc;
    }, {});
  saveToLc(result);
  return result;
}

function loadInitItems() {
  const localStorageVal = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY));
  if (localStorageVal && localStorageVal.length) {
    return localStorageVal.reduce((acc, x) => {
      acc[x.id] = x;
      return acc;
    }, {});
  }
  return loadAndPopulateDefaultItems();
}

function createProviderToggleStore() {
  const itemsById = loadInitItems();

  return {
    namespaced: true,
    state: {
      itemsById,
    },
    mutations: {
      setProviderEnabledState(state, obj) {
        state.itemsById = {
          ...state.itemsById,
          [obj.id]: obj,
        };
        saveToLc(state.itemsById);
      },
    },
    getters: {
      providerEnabled: (state) => (id) => {
        if (!id) {
          return false;
        }
        return !!state.itemsById?.[id]?.enabled;
      },
    },
    actions: {
      setProviderEnabledState({ commit, state }, obj) {
        commit('setProviderEnabledState', obj);
      },
    },
  };
}

export { createProviderToggleStore, LOCAL_STORAGE_KEY };

/* global require */
/* eslint-disable import/no-dynamic-require, global-require */

const STREAM = 'stream';
const RENT = 'rent';

const providers = [
  {
    id: 2,
    name: 'apple',
    img: 'apple',
    type: RENT,
    enabled: true,
  },
  { id: 3, name: 'google play store' },
  {
    id: 7,
    name: 'vudu',
    img: 'vudu',
    type: RENT,
    enabled: true,
  },
  {
    id: 8,
    name: 'netflix',
    img: 'netflix',
    type: STREAM,
    enabled: true,
  },
  {
    id: 9,
    name: 'amazon prime',
    img: 'amazon',
    type: STREAM,
    enabled: true,
  },
  {
    id: 10,
    name: 'amazon video',
    img: 'amazon',
    type: RENT,
  },
  { id: 14, name: 'realeyz' },
  {
    id: 15,
    name: 'hulu',
    img: 'hulu',
    type: STREAM,
    enabled: true,
  },
  { id: 18, name: 'playstation' },
  { id: 25, name: 'fandor.co' },
  { id: 27, name: 'hbonow' },
  {
    id: 31,
    name: 'hbo-go',
    img: 'hbo-go',
    type: STREAM,
    enabled: true,
  },
  { id: 34, name: 'epix' },
  { id: 37, name: 'showtime' },
  {
    id: 43,
    name: 'starz',
    img: 'starz',
    type: STREAM,
    enabled: true,
  },
  { id: 68, name: 'microsoft' },
  { id: 99, name: 'shudder' },
  { id: 102, name: 'filmstruck' },
  { id: 105, name: 'fandango now' },
  { id: 123, name: 'fxnetworks' },
  { id: 139, name: 'maxgo' },
  { id: 143, name: 'sundancenow.com' },
  { id: 185, name: 'screambox.com' },
  {
    id: 192,
    name: 'youtube',
    img: 'youtube',
    type: RENT,
    enabled: true,
  },
  {
    id: 279,
    name: 'redbox',
    img: 'redbox',
    type: RENT,
    enabled: true,
  },
  {
    id: 332,
    name: 'vudu free',
    img: 'vudu',
    type: STREAM,
    enabled: true,
  },
];

function getProviderIcon(id) {
  const item = providers.filter((x) => x.id === id);
  try {
    const { img } = item[0];
    if (img === undefined) {
      throw new Error();
    }
    return require(`@/images/provider-logos/${
      img // breaks jest when on one line
    }.svg`);
  } catch (e) {
    throw new Error('img not found');
  }
}

function isProviderIncluded(id) {
  return providers.some(
    (x) => x.id === id && Object.hasOwnProperty.call(x, 'type'),
  );
}

function getProvidersByType(type = 'ALL') {
  if (type === 'ALL') {
    return providers.filter((x) => Object.hasOwnProperty.call(x, 'type'));
  }
  return providers.filter((x) => x?.type === type);
}

function getProviderName(id) {
  const item = providers.filter((x) => x.id === id);
  return item.length ? item[0].name : '';
}

export {
  providers,
  getProviderIcon,
  getProviderName,
  isProviderIncluded,
  getProvidersByType,
  STREAM,
  RENT,
};

import { checkStatus, parseJSON } from 'fetch-json-helpers';
import config from 'config';
import { isProviderIncluded } from './providers';

function checkProviderEnabled(type, justwatchResponse, providerEnabled) {
  return (
    justwatchResponse.offers
    && Object.values(justwatchResponse.offers).some(
      (x) => x.monetization_type === type && providerEnabled(x.provider_id),
    )
  );
}

function canStream(justwatchResponse) {
  return (
    justwatchResponse.offers
    && Object.values(justwatchResponse.offers).some(
      (x) => (x.monetization_type === 'flatrate' || x.monetization_type === 'ads')
        && isProviderIncluded(x.provider_id),
    )
  );
}

function canRent(justwatchResponse) {
  return (
    justwatchResponse.offers
    && Object.values(justwatchResponse.offers).some(
      (x) => x.monetization_type === 'rent' && isProviderIncluded(x.provider_id),
    )
  );
}

function isJustwatchPopulated(item) {
  return (
    !!item?.justwatchResponse
    && !Object.hasOwnProperty.call(item.justwatchResponse, 'notFound')
  );
}

function isJustwatchNotFound(item) {
  return !!item?.justwatchResponse?.notFound;
}

function oldestOfferTime(item, type) {
  return Object.values(item.justwatchResponse.offers).reduce((acc, offer) => {
    if (offer.monetization_type === type) {
      const time = new Date(offer.date_created).getTime();
      if (time < acc) {
        return time;
      }
    }
    return acc;
  }, new Date().getTime());
}

function createQueueStore({ authService, queueService }) {
  return {
    namespaced: true,
    state: {
      items: [],
      fetching: false,
      justwatchFetching: {},
    },
    mutations: {
      updateItems(state, data) {
        state.items = data;
      },
      bgUpdateItems(state, data) {
        // not showing adds on bg refresh, makes the page jump
        // const adds = data.filter(
        //     x => !state.items.some(y => y.netflixId === x.netflixId),
        // );
        const removes = state.items.filter(
          (x) => !data.some((y) => y.netflixId === x.netflixId),
        );
        state.items = [
          ...data,
          ...removes.map((x) => {
            x.removing = true;
            return x;
          }),
        ];
      },
      fetching(state, data) {
        state.fetching = data;
      },
      justwatchFetching(state, { fetching, netflixId }) {
        state.justwatchFetching = {
          ...state.justwatchFetching,
          [netflixId]: fetching,
        };
      },
      updateJustwatch(state, { netflixId, justwatchId, result }) {
        const index = state.items.findIndex((x) => x.netflixId === netflixId);
        const newItem = {
          ...state.items[index],
          justwatchResponse: result,
          justwatchId: justwatchId === 'unset' ? '' : justwatchId,
          updatedAt: new Date().toISOString(),
          updated: true,
        };
        state.items.splice(index, 1, newItem);
      },
    },
    actions: {
      fetchItems({ commit, state, rootState }) {
        commit('fetching', true);
        return queueService
          .getQueue()
          .then((result) => {
            commit('updateItems', result);
            commit('fetching', false);
          })
          .catch((err) => {
            // eslint-disable-next-line no-console
            console.error(err);
            commit('fetching', false);
          });
      },
      async justwatch(
        { commit, state, rootState },
        { netflixId, justwatchId, objectType },
      ) {
        commit('justwatchFetching', { fetching: true, netflixId });
        const identityId = await authService.loadIdentityId();
        const idToken = await authService.loadIdtoken();
        const urlJustwatch = justwatchId ? `&justwatchId=${justwatchId}` : '';
        const urlObjType = objectType ? `&objectType=${objectType}` : '';
        const url = `${config.AWS.apiGatewayUrl}/queue/${identityId}/justwatch?netflixId=${netflixId}${urlJustwatch}${urlObjType}`;
        return fetch(url, {
          headers: new Headers({
            Authorization: `Bearer ${idToken}`,
            'Content-Type': 'application/json',
          }),
        })
          .then(checkStatus)
          .then(parseJSON)
          .then((result) => {
            commit('updateJustwatch', {
              netflixId,
              justwatchId,
              result,
            });
            commit('justwatchFetching', {
              fetching: false,
              netflixId,
            });
          })
          .catch((err) => {
            // eslint-disable-next-line no-console
            console.error(err);
            commit('justwatchFetching', {
              fetching: err,
              netflixId,
            });
          });
      },
      logout({ commit, state, rootState }) {
        commit('updateItems', []);
      },
      login({ commit, state, rootState }) {
        commit('updateItems', []);
      },
    },
    getters: {
      items: (state) => state.items,
      fetching: (state) => state.fetching,
      justwatchFetching: (state) => (id) => {
        if (!id) {
          return false;
        }
        return state?.justwatchFetching?.[id] ?? false;
      },
      populatedItems: (state) => state.items.filter((x) => isJustwatchPopulated(x)),
      unpopulatedItems: (state) => state.items.filter((x) => !x?.justwatchResponse),
      notFoundItems: (state) => state.items.filter((x) => isJustwatchNotFound(x)),
      streamableItems: (state, getters, rootState, rootGetters) => state.items
        .filter(
          (x) => isJustwatchPopulated(x) && canStream(x.justwatchResponse),
        )
        .filter(
          (x) => checkProviderEnabled(
            'flatrate',
            x.justwatchResponse,
            rootGetters['providerToggle/providerEnabled'],
          )
              || checkProviderEnabled(
                'ads',
                x.justwatchResponse,
                rootGetters['providerToggle/providerEnabled'],
              ),
        )
        .sort(
          (a, b) => oldestOfferTime(a, 'flatrate') - oldestOfferTime(b, 'flatrate'),
        )
        .reverse(),
      rentableItems: (state, getters, rootState, rootGetters) => state.items
        .filter(
          (x) => isJustwatchPopulated(x)
              && !canStream(x.justwatchResponse)
              && canRent(x.justwatchResponse),
        )
        .filter((x) => checkProviderEnabled(
          'rent',
          x.justwatchResponse,
          rootGetters['providerToggle/providerEnabled'],
        ))
        .sort(
          (a, b) => oldestOfferTime(a, 'rent') - oldestOfferTime(b, 'rent'),
        )
        .reverse(),
      nothingItems: (state) => state.items.filter(
        (x) => isJustwatchPopulated(x)
            && !canStream(x.justwatchResponse)
            && !canRent(x.justwatchResponse),
      ),
    },
  };
}

export { createQueueStore };

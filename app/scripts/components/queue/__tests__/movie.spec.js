import { shallowMount, createLocalVue, mount } from '@vue/test-utils';
import Vuex from 'vuex';
import nock from 'nock';
import config from 'config';
import Movie from '../movie';
import createStore from '../../../store';
import authService, {
  identityId,
} from '../../../service/__mocks__/auth-service';

let store;
let localVue;

let movie;

const notFoundMovie = {
  justwatchResponse: {
    notFound: 123,
  },
  title: 'Movie 1',
  netflixId: '456',
  updatedAt: '2018-02-17T05:24:14.537Z',
};

describe('movie', () => {
  beforeEach(() => {
    movie = {
      justwatchResponse: {
        original_release_year: 2015,
        scoring: [
          { provider_type: 'imdb:score', value: 352208 },
          { provider_type: 'metacritic:score', value: 352208 },
          { provider_type: 'tomato:meter', value: 352208 },
          { provider_type: 'tomato_userrating:meter', value: 352208 },
        ],
      },
      title: 'Movie 1',
      netflixId: '456',
      updatedAt: '2018-02-17T05:24:14.537Z',
    };
    store = createStore({ authService });
    localVue = createLocalVue();
    Object.defineProperty(window, 'matchMedia', {
      value: jest.fn(() => ({ matches: false })),
      writable: true,
    });
  });

  it('renders', () => {
    const sut = shallowMount(Movie, {
      store,
      localVue,
      propsData: {
        ...movie,
      },
    });
    expect(sut.find('.movie__title').text()).toContain(movie.title);
  });

  it('shows a question mark for score when its missing', () => {
    movie.justwatchResponse.scoring = [];
    const sut = shallowMount(Movie, {
      store,
      localVue,
      propsData: {
        ...movie,
      },
    });
    expect(sut.find('.movie__score--imdb').text()).toContain('?');
  });

  it('shows a score', () => {
    const sut = shallowMount(Movie, {
      store,
      localVue,
      propsData: {
        ...movie,
      },
    });
    expect(sut.find('.movie__score--imdb').text()).toContain('352208');
  });

  it('can handle scoring being an object instead of an array', () => {
    movie.justwatchResponse.scoring = movie.justwatchResponse.scoring.reduce(
      (acc, x, i) => {
        acc[i] = x;
        return acc;
      },
      {},
    );
    const sut = shallowMount(Movie, {
      store,
      localVue,
      propsData: {
        ...movie,
      },
    });
    expect(sut.find('.movie__score--imdb').text()).toContain('352208');
  });

  it('shows a "populate" button when there is no justwatchResponse', () => {
    delete movie.justwatchResponse;
    const sut = shallowMount(Movie, {
      store,
      localVue,
      propsData: {
        ...movie,
      },
    });
    expect(sut.find('.movie__refresh-button').text()).toContain('Populate');
  });

  describe('content size', () => {
    it('hides the content in mobile', () => {
      const sut = shallowMount(Movie, {
        store,
        localVue,
        propsData: {
          ...movie,
        },
      });
      expect(sut.find('.movie__content').element.style.height).toBe('0px');
    });
    it('toggles the content in movie', async () => {
      Object.defineProperties(window.HTMLElement.prototype, {
        offsetHeight: {
          get() {
            return 400;
          },
        },
        offsetWidth: {
          get() {
            return 200;
          },
        },
      });
      const sut = shallowMount(Movie, {
        store,
        localVue,
        propsData: {
          ...movie,
        },
      });
      sut.find('.movie__toggle-info-btn').trigger('click');
      await localVue.nextTick();
      expect(sut.find('.movie__content').element.style.height).toBe('400px');
    });
    it('shows the content in desktop', () => {
      Object.defineProperty(window, 'matchMedia', {
        value: jest.fn(() => ({ matches: true })),
      });
      const sut = shallowMount(Movie, {
        store,
        localVue,
        propsData: {
          ...movie,
        },
      });
      expect(sut.find('.movie__content').element.style.height).toBe('auto');
    });

    it('sets the height after img load', async () => {
      Object.defineProperties(window.HTMLElement.prototype, {
        offsetHeight: {
          get() {
            return 222;
          },
        },
      });
      Object.defineProperties(window.Image.prototype, {
        complete: {
          get() {
            return true;
          },
        },
      });
      movie.justwatchResponse.offers = [
        {
          monetization_type: 'flatrate',
          provider_id: 2,
          retail_price: 3,
          urls: {
            standard_web: 'https://www.starz.com/movies/34773',
          },
        },
        {
          monetization_type: 'flatrate',
          provider_id: 2,
          retail_price: 2,
          urls: {
            standard_web: 'https://www.starz.com/movies/34773',
          },
        },
      ];

      const sut = mount(Movie, {
        store,
        localVue,
        propsData: {
          ...movie,
        },
      });
      sut.setData({ showing: true });
      sut.vm.resizeContent();
      await localVue.nextTick();
      expect(sut.find('.movie__content').element.style.height).toBe('222px');
    });
  });

  it('has a year when justwatch response is specified', () => {
    const sut = shallowMount(Movie, {
      store,
      localVue,
      propsData: {
        ...movie,
      },
    });
    expect(sut.find('.movie__title').text()).toContain(
      movie.justwatchResponse.original_release_year,
    );
  });

  it('does not have a year when justwatch is not specified', () => {
    const sut = shallowMount(Movie, {
      store,
      localVue,
      propsData: {
        ...notFoundMovie,
      },
    });
    expect(sut.find('.movie__title').text()).not.toContain('(');
  });

  it('shows not found notice', () => {
    const sut = shallowMount(Movie, {
      store,
      localVue,
      propsData: {
        ...notFoundMovie,
      },
    });
    expect(sut.findAll('.movie__not-found-notice').length).toBe(1);
  });

  it('shows a score', () => {
    const sut = shallowMount(Movie, {
      store,
      localVue,
      propsData: {
        ...movie,
      },
    });
    expect(sut.find('.movie__score--imdb').text()).toBe('imdb: 352208');
  });

  it('calls though to refresh', async () => {
    const actions = {
      'queue/justwatch': jest.fn(),
    };
    store = new Vuex.Store({
      state: {},
      actions,
      getters: {
        'queue/justwatchFetching': () => () => false,
        'authenticated/isAuthenticated': () => true,
      },
    });
    const sut = shallowMount(Movie, {
      store,
      localVue,
      propsData: {
        ...movie,
      },
    });
    sut.find('.movie__refresh-button').trigger('click');
    await localVue.nextTick();
    expect(actions['queue/justwatch'].mock.calls).toHaveLength(1);
    expect(actions['queue/justwatch'].mock.calls[0][1]).toEqual({
      netflixId: movie.netflixId,
      objectType: '',
    });
  });

  it('calls sends a justwatchId', () => {
    const actions = {
      'queue/justwatch': jest.fn(),
    };
    store = new Vuex.Store({
      state: {},
      actions,
      getters: {
        'queue/justwatchFetching': () => () => false,
        'authenticated/isAuthenticated': () => true,
      },
    });
    const sut = shallowMount(Movie, {
      store,
      localVue,
      propsData: {
        ...movie,
      },
    });
    sut.setData({ justwatchIdInputVal: 'foo' });
    sut.find('.movie__refresh-button').trigger('click');
    expect(actions['queue/justwatch'].mock.calls).toHaveLength(1);
    expect(actions['queue/justwatch'].mock.calls[0][1]).toEqual({
      netflixId: movie.netflixId,
      justwatchId: 'foo',
      objectType: '',
    });
  });

  it('calls sets justwatchId to unset', async () => {
    const actions = {
      'queue/justwatch': jest.fn(),
    };
    store = new Vuex.Store({
      state: {},
      actions,
      getters: {
        'queue/justwatchFetching': () => () => false,
        'authenticated/isAuthenticated': () => true,
      },
    });
    movie.justwatchId = '2222';
    const sut = shallowMount(Movie, {
      store,
      localVue,
      propsData: {
        ...movie,
      },
    });
    sut.setData({ justwatchIdInputVal: '' });
    sut.find('.movie__refresh-button').trigger('click');
    await localVue.nextTick();
    expect(actions['queue/justwatch'].mock.calls).toHaveLength(1);
    expect(actions['queue/justwatch'].mock.calls[0][1]).toEqual({
      netflixId: movie.netflixId,
      justwatchId: 'unset',
      objectType: '',
    });
  });

  it('shows an updated date', () => {
    const sut = shallowMount(Movie, {
      store,
      localVue,
      propsData: {
        ...movie,
      },
    });
    expect(sut.find('.movie__update-date').text()).toContain('Feb 16 2018');
  });

  it('can have a forced justwatchid', async () => {
    const sut = shallowMount(Movie, {
      store,
      localVue,
      propsData: {
        ...movie,
        justwatchId: '123',
      },
    });
    await localVue.nextTick();
    expect(sut.find(`#justwatchId-${movie.netflixId}`).element.value).toBe(
      '123',
    );
  });

  it('shows loading while refreshing justwatch data', async () => {
    const actions = {
      'queue/justwatch': jest.fn(),
    };
    store = new Vuex.Store({
      state: {},
      actions,
      getters: {
        'queue/justwatchFetching': () => () => true,
      },
    });
    const sut = shallowMount(Movie, {
      store,
      localVue,
      propsData: {
        ...movie,
        justwatchId: '123',
      },
    });
    expect(sut.find('.loading--placeholder').text()).toContain('Refreshing');
  });

  it('shows an error when justwatch fails', async () => {
    // eslint-disable-next-line no-console
    console.error = jest.fn();
    nock.disableNetConnect();
    nock(`${config.AWS.apiGatewayUrl}/`)
      .get(`/queue/${identityId}/justwatch`)
      .query({
        netflixId: 456,
      })
      .replyWithError({
        message: 'something awful happened',
        code: 'AWFUL_ERROR',
      });
    const sut = shallowMount(Movie, {
      store,
      localVue,
      propsData: {
        ...movie,
      },
    });
    await sut.vm.onRefreshClicked();
    await localVue.nextTick();
    expect(sut.findAll('.loading--placeholder').length).toBe(0);
    expect(sut.find('.error').text()).toContain('something awful happened');
  });
});

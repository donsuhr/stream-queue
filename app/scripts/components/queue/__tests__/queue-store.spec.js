/* eslint-env jest */

import nock from 'nock';
import config from 'config';
import { createQueueStore } from '../queue-store';
import authService, {
  identityId,
} from '../../../service/__mocks__/auth-service';

let sut;

describe('queue-store', () => {
  beforeEach(() => {
    sut = createQueueStore({ authService });
  });

  describe('mutations', () => {
    it('updateItems', () => {
      const items = ['one', 'two'];
      expect(sut.state.items).toEqual([]);
      sut.mutations.updateItems(sut.state, items);
      expect(sut.state.items).toEqual(items);
    });

    it('putting', () => {
      const someError = new Error('failed');
      expect(sut.state.fetching).toBe(false);
      sut.mutations.fetching(sut.state, true);
      expect(sut.state.fetching).toBe(true);
      sut.mutations.fetching(sut.state, someError);
      expect(sut.state.fetching).toBe(someError);
    });

    it('justwatchFetching', () => {
      expect(sut.state.justwatchFetching).toEqual({});
      sut.mutations.justwatchFetching(sut.state, {
        fetching: true,
        netflixId: 123,
      });
      expect(sut.state.justwatchFetching).toEqual({ 123: true });
    });

    it('justwatchFetching can be an error', () => {
      expect(sut.state.justwatchFetching).toEqual({});
      const error = new Error('foo');
      sut.mutations.justwatchFetching(sut.state, {
        fetching: error,
        netflixId: 123,
      });
      expect(sut.state.justwatchFetching).toEqual({ 123: error });
    });

    it('updateJustwatch', () => {
      sut.state.items = [{ netflixId: 123, updated: false }];
      sut.mutations.updateJustwatch(sut.state, {
        netflixId: 123,
        result: {},
      });
      expect(sut.state.items[0].updated).toEqual(true);
      expect(sut.state.items[0].netflixId).toEqual(123);
      expect(sut.state.items[0].justwatchResponse).toEqual({});
    });

    it('doesnt set the justwatchId field to "unset"', () => {
      sut.state.items = [{ netflixId: 123, updated: false }];
      sut.mutations.updateJustwatch(sut.state, {
        netflixId: 123,
        justwatchId: 'unset',
        result: {},
      });
      expect(sut.state.items[0].justwatchId).toEqual('');
    });

    it('sets the justwatchId', () => {
      sut.state.items = [{ netflixId: 123, updated: false }];
      sut.mutations.updateJustwatch(sut.state, {
        netflixId: 123,
        justwatchId: '456',
        result: {},
      });
      expect(sut.state.items[0].justwatchId).toEqual('456');
    });

    it('bgUpdateItems', () => {
      const refreshedQueue = [
        {
          netflixId: 123,
        },
      ];
      sut.state.items = [{ netflixId: 123 }, { netflixId: 234 }];
      sut.mutations.bgUpdateItems(sut.state, refreshedQueue);
      const removedItem = sut.state.items.find((x) => x.netflixId === 234);
      expect(removedItem.removing).toBe(true);
    });
  });

  describe('getters', () => {
    const gettersTestState = {
      items: [
        {
          justwatchResponse: {
            offers: [
              {
                monetization_type: 'flatrate',
                provider_id: 15,
                date_created: '2018-12-21',
              },
              {
                monetization_type: 'flatrate',
                provider_id: 15,
                date_created: '2018-12-20',
              },
            ],
          },
        },
        {
          justwatchResponse: {
            offers: [
              {
                monetization_type: 'flatrate',
                provider_id: 15,
                date_created: '2018-12-20',
              },
              {
                monetization_type: 'flatrate',
                provider_id: 15,
                date_created: '2018-12-21',
              },
            ],
          },
        },
        {},
        { justwatchResponse: { notFound: 123 } },
        {
          justwatchResponse: {
            offers: [
              {
                monetization_type: 'rent',
                provider_id: 7,
              },
            ],
          },
        },
        {
          justwatchResponse: {
            offers: [
              {
                monetization_type: 'rent',
                provider_id: 3,
              },
            ],
          },
        },
        {
          justwatchResponse: {
            offers: [
              {
                monetization_type: 'rent',
                provider_id: 14,
              },
            ],
          },
        },
        {
          justwatchResponse: {
            offers: [
              {
                monetization_type: 'rent',
                provider_id: 14,
              },
            ],
          },
        },
      ],
    };

    it('fetching', () => {
      const state = {
        fetching: false,
      };
      expect(sut.getters.fetching(state)).toBe(false);
    });

    it('items', () => {
      const state = { items: ['one', 'two'] };
      expect(sut.getters.items(state)).toEqual(state.items);
    });

    it('justwatchFetching', () => {
      let state = {
        justwatchFetching: {},
      };
      expect(sut.getters.justwatchFetching(state)(2)).toBe(false);
      state = {
        justwatchFetching: { 2: true },
      };
      expect(sut.getters.justwatchFetching(state)(2)).toBe(true);
      expect(sut.getters.justwatchFetching(state)(3)).toBe(false);
      expect(sut.getters.justwatchFetching(state)()).toBe(false);
    });

    it('populatedItems', () => {
      expect(sut.getters.populatedItems(gettersTestState).length).toBe(6);
    });
    it('unpopulatedItems', () => {
      expect(sut.getters.unpopulatedItems(gettersTestState).length).toBe(1);
    });
    it('notFoundItems', () => {
      expect(sut.getters.notFoundItems(gettersTestState).length).toBe(1);
    });
    it('streamableItems', () => {
      const rootGetters = {
        'providerToggle/providerEnabled': () => true,
      };
      expect(
        sut.getters.streamableItems(gettersTestState, null, null, rootGetters)
          .length,
      ).toBe(2);
    });
    it('rentableItems', () => {
      const rootGetters = {
        'providerToggle/providerEnabled': () => true,
      };
      expect(
        sut.getters.rentableItems(gettersTestState, null, null, rootGetters)
          .length,
      ).toBe(1);
    });
    it('nothingItems', () => {
      expect(sut.getters.nothingItems(gettersTestState).length).toBe(3);
    });
  });

  describe('actions', () => {
    it('fetchItems', async () => {
      nock.disableNetConnect();
      const commit = jest.fn();
      const state = {
        items: [],
      };
      const rootState = {};
      const result = {
        items: [
          { title: 'Movie 1', netflixId: 123, justwatchResponse: {} },
          { title: 'Movie 2', netflixId: 345, justwatchResponse: {} },
        ],
      };

      const queueService = {
        getQueue: () => Promise.resolve(result.items),
      };
      sut = createQueueStore({ authService, queueService });

      await sut.actions.fetchItems({ commit, state, rootState });
      expect(commit.mock.calls[0][0]).toBe('fetching');
      expect(commit.mock.calls[0][1]).toBe(true);
      expect(commit.mock.calls[1][0]).toBe('updateItems');
      expect(commit.mock.calls[1][1]).toEqual(result.items);
      expect(commit.mock.calls[2][0]).toBe('fetching');
      expect(commit.mock.calls[2][1]).toBe(false);
    });

    it('turns off the loading on error', async () => {
      const commit = jest.fn();
      const state = {
        items: [],
      };
      const rootState = {};
      const queueService = {
        getQueue: () => Promise.reject(new Error('foo')),
      };
      sut = createQueueStore({ authService, queueService });
      // eslint-disable-next-line no-console
      console.error = jest.fn();
      await sut.actions.fetchItems({ commit, state, rootState });
      expect(commit.mock.calls[0][0]).toBe('fetching');
      expect(commit.mock.calls[0][1]).toBe(true);
      expect(commit.mock.calls[1][0]).toBe('fetching');
      expect(commit.mock.calls[1][1]).toBe(false);
    });

    it('justwatch', async () => {
      nock.disableNetConnect();
      const commit = jest.fn();
      const state = {
        items: [],
      };
      const rootState = {};
      const result = { foo: 'bar' };

      nock(`${config.AWS.apiGatewayUrl}/`)
        .get(`/queue/${identityId}/justwatch`)
        .query({
          netflixId: 11,
        })
        .reply(200, result);

      await sut.actions.justwatch(
        { commit, state, rootState },
        { netflixId: 11 },
      );
      expect(commit.mock.calls[0][0]).toBe('justwatchFetching');
      expect(commit.mock.calls[0][1]).toEqual({
        fetching: true,
        netflixId: 11,
      });
      expect(commit.mock.calls[1][0]).toBe('updateJustwatch');
      expect(commit.mock.calls[1][1]).toEqual({
        netflixId: 11,
        justwatchId: undefined,
        result,
      });
      expect(commit.mock.calls[2][0]).toBe('justwatchFetching');
      expect(commit.mock.calls[2][1]).toEqual({
        fetching: false,
        netflixId: 11,
      });
    });

    it('adds justwatch query param', async () => {
      nock.disableNetConnect();
      const commit = jest.fn();
      const state = {
        items: [],
      };
      const rootState = {};
      const result = { foo: 'bar' };

      const scope = nock(`${config.AWS.apiGatewayUrl}/`)
        .get(`/queue/${identityId}/justwatch`)
        .query({
          netflixId: 11,
          justwatchId: 111,
        })
        .reply(200, result);

      await sut.actions.justwatch(
        { commit, state, rootState },
        { netflixId: 11, justwatchId: 111 },
      );
      expect(scope.isDone()).toBe(true);
    });

    it('justwatch turns off loading when err', async () => {
      nock.disableNetConnect();
      const commit = jest.fn();
      const state = {};
      const rootState = {};

      nock(`${config.AWS.apiGatewayUrl}/`)
        .get(`/queue/${identityId}/justwatch`)
        .query({
          netflixId: 33,
        })
        .reply(500);

      // eslint-disable-next-line no-console
      console.error = jest.fn();

      await sut.actions.justwatch(
        { commit, state, rootState },
        { netflixId: 33 },
      );
      expect(commit.mock.calls[1][0]).toBe('justwatchFetching');
      expect(commit.mock.calls[1][1].netflixId).toEqual(33);
      expect(commit.mock.calls[1][1].fetching).toBeInstanceOf(Error);
    });

    it('clears the queue on logout', async () => {
      const commit = jest.fn();
      const state = {};
      const rootState = {};
      sut = createQueueStore({ authService, queueService: {} });
      sut.actions.logout({ commit, state, rootState });
      expect(commit.mock.calls[0][0]).toBe('updateItems');
      expect(commit.mock.calls[0][1]).toEqual([]);
    });

    it('clears the queue on login', async () => {
      const commit = jest.fn();
      const state = {};
      const rootState = {};
      sut = createQueueStore({ authService, queueService: {} });
      sut.actions.login({ commit, state, rootState });
      expect(commit.mock.calls[0][0]).toBe('updateItems');
      expect(commit.mock.calls[0][1]).toEqual([]);
    });
  });
});

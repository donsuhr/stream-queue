/* eslint-env jest */
import { shallowMount, createLocalVue } from '@vue/test-utils';
import QueueGrid from '../queue-grid';
import createStore from '../../../store';
import authService from '../../../service/__mocks__/auth-service';

let localVue;
let store;

const items = [
  {
    _id: '5a85ea1206f3df5a696bc34f',
    netflixId: '80116741',
    title: 'My Life as a Zucchini',
    updatedAt: '2018-02-17T05:29:40.492Z',
    justwatchResponse: {},
  },
  {
    _id: '5a85ea1206f3df5a696bc34g',
    netflixId: '801167412',
    title: 'My Life as a Zucchini2',
    updatedAt: '2018-02-17T05:29:40.492Z',
    justwatchResponse: {},
  },
];

describe('QueueGrid', () => {
  beforeEach(() => {
    localVue = createLocalVue();
    store = createStore({ authService });
  });

  it('renders', () => {
    const sut = shallowMount(QueueGrid, {
      store,
      localVue,
      propsData: {
        items,
      },
    });
    expect(sut.findAll('aggridvue-stub').length).toBe(1);
  });

  it('handles unset justwatchResponse', () => {
    expect(() => shallowMount(QueueGrid, {
      store,
      localVue,
      propsData: {
        items: [
          {
            _id: '5a85ea1206f3df5a696bc34f',
            netflixId: '80116741',
            title: 'My Life as a Zucchini',
            updatedAt: '2018-02-17T05:29:40.492Z',
          },
        ],
      },
    })).not.toThrow();
  });
});

import { shallowMount, mount, createLocalVue } from '@vue/test-utils';
import ProviderToggle from '../provider-toggle';
import createStore from '../../../store';
import authService from '../../../service/__mocks__/auth-service';

let localVue;
let store;

describe('ProviderIcon', () => {
  beforeEach(() => {
    localVue = createLocalVue();
    store = createStore({ authService });
  });

  it('renders', () => {
    store.replaceState({
      providerToggle: {
        itemsById: { 2: { id: 2, enabled: true } },
      },
    });
    const name = 'Name';
    const sut = shallowMount(ProviderToggle, {
      store,
      localVue,
      propsData: {
        name,
        id: 2,
      },
    });
    expect(sut.find('.provider-toggle').text()).toContain(name);
  });

  it('starts with active set to true', () => {
    store.replaceState({
      providerToggle: {
        itemsById: { 2: { id: 2, enabled: true } },
      },
    });
    const sut = mount(ProviderToggle, {
      store,
      localVue,
      propsData: {
        name: 'Name',
        id: 2,
      },
    });
    expect(sut.vm.active).toBe(true);
  });

  it('starts with active set to false', () => {
    store.replaceState({
      providerToggle: {
        itemsById: { 2: { id: 2, enabled: true } },
      },
    });
    const sut = mount(ProviderToggle, {
      store,
      localVue,
      propsData: {
        name: 'Name',
        id: 3,
      },
    });
    expect(sut.vm.active).toBe(false);
  });

  it('toggles active when clicked', async () => {
    const sut = mount(ProviderToggle, {
      store,
      localVue,
      propsData: {
        name: 'Name',
        id: 3,
      },
    });
    expect(sut.vm.active).toBe(false);
    sut.find('button').trigger('click');
    await localVue.nextTick();
    expect(sut.vm.active).toBe(true);
  });
});

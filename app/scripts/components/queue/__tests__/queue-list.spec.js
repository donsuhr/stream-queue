/* eslint-env jest */

import { shallowMount, createLocalVue } from '@vue/test-utils';
import QueueList from '../queue-list';
import createStore from '../../../store';
import authService from '../../../service/__mocks__/auth-service';

let localVue;
let store;

const items = [
  {
    _id: '5a85ea1206f3df5a696bc34f',
    netflixId: '80116741',
    title: 'My Life as a Zucchini',
    updatedAt: '2018-02-17T05:29:40.492Z',
  },
  {
    _id: '5a85ea1206f3df5a696bc34g',
    netflixId: '801167412',
    title: 'My Life as a Zucchini2',
    updatedAt: '2018-02-17T05:29:40.492Z',
  },
];

describe('QueueList', () => {
  beforeEach(() => {
    localVue = createLocalVue();
    store = createStore({ authService });
  });

  it('renders', () => {
    const $route = { params: { filter: 'all' } };
    store.replaceState({
      queue: {
        items,
      },
    });

    const sut = shallowMount(QueueList, {
      store,
      localVue,
      mocks: {
        $route,
      },
      stubs: ['RouterLink'],
      propsData: {
        items,
      },
    });
    expect(sut.findAll('.queue-list li').length).toBe(2);
  });

  it('shows loading', async () => {
    const $route = { params: { filter: 'all' } };
    store.replaceState({
      queue: {
        fetching: true,
        items,
      },
    });

    const sut = shallowMount(QueueList, {
      store,
      localVue,
      mocks: {
        $route,
      },
      stubs: ['RouterLink'],
    });
    await localVue.nextTick();
    expect(sut.find('.loading--placeholder').text()).toBe('Loading...');
  });
});

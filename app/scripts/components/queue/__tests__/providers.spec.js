/* eslint-disable import/no-dynamic-require, global-require */

import {
  getProviderIcon,
  isProviderIncluded,
  getProviderName,
  getProvidersByType,
  STREAM,
} from '../providers';

describe('providers', () => {
  it('can get a provider icon', () => {
    const img = require('@/images/provider-logos/apple.svg');
    expect(getProviderIcon(2)).toEqual(img);
  });

  it('throws when accessing a provider with no img', () => {
    expect(() => {
      getProviderIcon(105);
    }).toThrow();
  });

  it('throws when accessing unknown provider', () => {
    expect(() => {
      getProviderIcon(666);
    }).toThrow();
  });

  it('performs a simple provider included test', () => {
    expect(isProviderIncluded(192)).toBe(true);
    expect(isProviderIncluded(185)).toBe(false);
  });

  it('returns a providers name', () => {
    expect(getProviderName(192)).toBe('youtube');
  });

  it('returns a list of providers', () => {
    const streamers = getProvidersByType(STREAM);
    const check = streamers.filter((x) => x.type === STREAM);
    expect(streamers.length).toBe(check.length);
  });
});

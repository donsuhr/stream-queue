import { shallowMount, createLocalVue } from '@vue/test-utils';
import ProviderIcon from '../provider-icon';

let localVue;

describe('ProviderIcon', () => {
  beforeEach(() => {
    localVue = createLocalVue();
  });

  it('renders', () => {
    jest.mock('@/images/provider-logos/vudu.svg', () => ({
      __esModule: true,
      default: '/images/provider-logos/vudu.svg',
    }));
    const sut = shallowMount(ProviderIcon, {
      localVue,
      propsData: {
        provider_id: 7,
        urls: {},
        retail_price: 3.99,
        offerType: 'match',
      },
    });
    expect(
      sut.find('.movie__service-availability-icon__img').element.src,
    ).toContain('/images/provider-logos/vudu.svg');
  });

  it('shows the price on a rental', () => {
    const sut = shallowMount(ProviderIcon, {
      localVue,
      propsData: {
        provider_id: 7,
        urls: {},
        retail_price: 3.99,
        offerType: 'rent',
      },
    });
    expect(
      sut.find('.movie__service-availability-icon__price').text(),
    ).toContain('3.99');
  });

  it('shows a question mark for unknown provider', () => {
    // eslint-disable-next-line no-console
    console.error = jest.fn();
    const sut = shallowMount(ProviderIcon, {
      localVue,
      propsData: {
        provider_id: 99999,
        urls: {},
        retail_price: 3.99,
        offerType: 'rent',
      },
    });
    expect(
      sut.find('.movie__service-availability-icon__img').element.src,
    ).toContain('question-mark');
  });
});

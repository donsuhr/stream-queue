import { shallowMount, createLocalVue } from '@vue/test-utils';
import OfferRow from '../offer-row';

let localVue;

const offers = [
  {
    monetization_type: 'match',
    provider_id: 2,
    retail_price: 3,
    urls: {
      standard_web: 'https://www.starz.com/movies/34773',
    },
  },
  {
    monetization_type: 'match',
    provider_id: 2,
    retail_price: 2,
    urls: {
      standard_web: 'https://www.starz.com/movies/34773',
    },
  },
  {
    monetization_type: 'nomatch',
    provider_id: 8,
    retail_price: 1,
    urls: {
      standard_web: 'https://www.starz.com/movies/34773',
    },
  },
];

describe('OfferRow', () => {
  beforeEach(() => {
    localVue = createLocalVue();
  });

  test('renders', () => {
    const sut = shallowMount(OfferRow, {
      localVue,
      propsData: {
        offers,
        offerType: 'match',
      },
    });
    expect(sut.findAll('.offer-row__item').length).toBe(1);
  });

  test('shows n/a when no match', () => {
    const sut = shallowMount(OfferRow, {
      localVue,
      propsData: {
        offers: [],
        offerType: 'match',
      },
    });
    expect(sut.findAll('.offer-row__not-available').length).toBe(1);
  });

  it('puts them in ascending price', () => {
    const sut = shallowMount(OfferRow, {
      localVue,
      propsData: {
        offers,
        offerType: 'match',
      },
    });
    expect(
      sut
        .findAll('.offer-row__item providericon-stub')
        .at(0)
        .attributes('retail_price'),
    ).toEqual('2');
  });
});

import {
  createProviderToggleStore,
  LOCAL_STORAGE_KEY,
} from '../provider-toggle-store';

import { getProvidersByType } from '../providers';

let sut;

describe('provider-toggle-store', () => {
  beforeEach(() => {
    localStorage.clear();
    sut = createProviderToggleStore();
  });

  it('loads its state from localStorage', () => {
    const state = [{ id: 2, enabled: true }, { id: 3, enabled: false }];
    localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(state));
    sut = createProviderToggleStore();
    expect(sut.state.itemsById[2].enabled).toBe(true);
  });

  it('enables all buttons when localStorage is empty', () => {
    const result = getProvidersByType()
      .map((x) => ({ id: x.id, enabled: true }))
      .reduce((acc, x) => {
        acc[x.id] = x;
        return acc;
      }, {});
    expect(sut.state.itemsById).toEqual(result);
  });

  describe('getters', () => {
    it('providerEnabled', () => {
      const state = { itemsById: { 2: { enabled: true, id: '2' } } };
      expect(sut.getters.providerEnabled(state)('2')).toEqual(
        state.itemsById['2'].enabled,
      );
    });
    it('providerEnabled no ID', () => {
      const state = {};
      expect(sut.getters.providerEnabled(state)()).toBe(false);
    });
  });

  describe('actions', () => {
    it('setProviderEnabled', () => {
      const commit = jest.fn();
      const obj = { id: 2, enabled: true };
      sut.actions.setProviderEnabledState({ commit }, obj);
      expect(commit.mock.calls).toHaveLength(1);
      expect(commit.mock.calls[0][1]).toEqual(obj);
    });
  });

  describe('mutations', () => {
    it('setProviderEnabledState', () => {
      const state = [
        { id: 2, enabled: false },
        { id: 3, enabled: false },
      ];
      localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(state));
      sut = createProviderToggleStore();
      const update = { id: 2, enabled: true };
      expect(sut.state.itemsById['2']).toEqual(state[0]);
      sut.mutations.setProviderEnabledState(sut.state, update);
      expect(sut.state.itemsById['2']).toEqual(update);
    });

    it('persists data to localStorage', () => {
      const state = [{ id: 2, enabled: true }, { id: 3, enabled: false }];
      localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(state));
      sut = createProviderToggleStore();
      const update = { id: 3, enabled: true };
      sut.mutations.setProviderEnabledState(sut.state, update);
      const lc = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY));
      expect(lc.find((x) => x.id === 3).enabled).toBe(true);
    });
  });
});

import fs from 'fs';
import path from 'path';
import util from 'util';
import nock from 'nock';
import Vuex from 'vuex';
import { shallowMount, mount, createLocalVue } from '@vue/test-utils';
import DomParserForm from '../dom-parser-form';
import createStore from '../../../store';
import authService from '../../../service/__mocks__/auth-service';

const localVue = createLocalVue();
const readFile = util.promisify(fs.readFile);
let store;

describe('dom-parser-form.vue', () => {
  beforeEach(() => {
    store = createStore({ authService });
    nock.disableNetConnect();
  });

  afterEach(() => {
    nock.cleanAll();
  });

  it('renders', () => {
    const sut = shallowMount(DomParserForm, {
      store,
      localVue,
    });
    expect(sut.find('label').text()).toContain('Paste');
  });

  it('connects the input to the preview', async () => {
    jest.useFakeTimers();
    const filePath = path.join(__dirname, 'que-item.html');
    const inputText = await readFile(filePath);
    const sut = mount(DomParserForm, {
      store,
      localVue,
    });
    const textArea = sut.find('textarea');
    textArea.element.value = inputText;
    textArea.trigger('input');
    jest.runAllTimers();
    await localVue.nextTick();
    const result = {
      items: [
        {
          netflixId: '81014833',
          title: 'Fahrenheit 11/9',
        },
      ],
    };
    expect(sut.find('.parsed-movie-list li').text()).toBe(
      result.items[0].title,
    );
  });

  it('shows parsing spinner when parsing', async () => {
    jest.useFakeTimers();
    const sut = shallowMount(DomParserForm, {
      store,
      localVue,
    });
    const ta = sut.find('textarea');
    ta.value = 'foo';
    ta.trigger('input');
    await localVue.nextTick();
    expect(sut.find('.loading--placeholder').text()).toBe('Parsing...');
    jest.runAllTimers();
    await localVue.nextTick();
    expect(sut.findAll('.loading--placeholder').length).toBe(0);
  });

  it('shows loading when posting', async () => {
    store.replaceState({
      parsedMovies: {
        putting: true,
      },
    });
    const sut = mount(DomParserForm, {
      store,
      localVue,
    });
    expect(sut.find('.loading--placeholder').text()).toBe('Posting...');
  });

  it('dispatches a movie list', async () => {
    jest.useFakeTimers();
    const actions = {
      'parsedMovies/setItems': jest.fn(),
      'parsedMovies/putItems': jest.fn(),
    };
    store = new Vuex.Store({
      state: {
        domDataAttributes: {},
        parsedMovies: {},
      },
      actions,
    });

    const sut = shallowMount(DomParserForm, {
      store,
      localVue,
    });
    const ta = sut.find('textarea');
    ta.element.value = `<div class="queue-item active" data-id="123">
                        <div class="title"><a>some title</a></div>
                    </div>
                    <div class="queue-item saved" data-id="456">
                        <div class="title"><a>some title 2</a></div>
                    </div>
                    `;
    ta.trigger('input');
    jest.runAllTimers();
    expect(actions['parsedMovies/setItems'].mock.calls).toHaveLength(1);
    expect(actions['parsedMovies/setItems'].mock.calls[0][1]).toEqual([
      { title: 'some title', netflixId: '123' },
      { title: 'some title 2', netflixId: '456' },
    ]);
  });

  it('calls put when clicking the button', () => {
    const actions = {
      'parsedMovies/setItems': jest.fn(),
      'parsedMovies/putItems': jest.fn(),
    };
    store = new Vuex.Store({
      state: {
        domDataAttributes: {},
        parsedMovies: {},
      },
      actions,
    });
    const sut = shallowMount(DomParserForm, {
      store,
      localVue,
    });
    sut.find('button').trigger('click');
    expect(actions['parsedMovies/putItems'].mock.calls).toHaveLength(1);
  });
});

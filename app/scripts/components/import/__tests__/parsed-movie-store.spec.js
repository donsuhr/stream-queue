/* eslint-env jest */

import { createParsedMovieStore } from '../parsed-movie-store';
import authService from '../../../service/__mocks__/auth-service';

let sut;

describe('parsed-movie-store', () => {
  beforeEach(() => {
    sut = createParsedMovieStore({});
  });

  it('creates a store', () => {
    expect('state' in sut).toBe(true);
    expect('items' in sut.state).toBe(true);
  });

  describe('mutations', () => {
    it('setItems', () => {
      const items = ['one', 'two'];
      expect(sut.state.items).toEqual([]);
      sut.mutations.setItems(sut.state, items);
      expect(sut.state.items).toEqual(items);
    });

    it('putting', () => {
      const someError = new Error('failed');
      expect(sut.state.putting).toBe(false);
      sut.mutations.putting(sut.state, true);
      expect(sut.state.putting).toBe(true);
      sut.mutations.putting(sut.state, someError);
      expect(sut.state.putting).toBe(someError);
    });
  });

  describe('getters', () => {
    it('putting', () => {
      const state = {
        putting: false,
      };
      expect(sut.getters.putting(state)).toBe(state.putting);
    });
  });

  describe('actions', () => {
    it('setItems', () => {
      const items = ['one', 'two'];
      const commit = jest.fn();
      sut.actions.setItems({ commit }, items);
      expect(commit.mock.calls).toHaveLength(1);
      expect(commit.mock.calls[0][1]).toEqual(items);
    });

    it('putItems', async () => {
      const router = {
        push: jest.fn(),
      };
      const commit = jest.fn();
      const state = {
        items: [],
      };
      const rootState = {
        domDataAttributes: {
          'queue-id': 123,
        },
      };
      const result = {
        items: [
          { title: 'Movie 1', netflixId: 123, justwatchResponse: {} },
          { title: 'Movie 2', netflixId: 345, justwatchResponse: {} },
        ],
      };

      const queueService = {
        patchQueue: () => Promise.resolve(result.items),
      };
      sut = createParsedMovieStore({ router, authService, queueService });
      await sut.actions.putItems({ commit, state, rootState });
      expect(commit.mock.calls[0][0]).toEqual('putting');
      expect(commit.mock.calls[0][1]).toBe(true);
      expect(commit.mock.calls[1][0]).toEqual('putting');
      expect(commit.mock.calls[1][1]).toBe(false);
      expect(router.push.mock.calls[0][0]).toEqual({
        name: 'queue',
        params: { filter: 'notset' },
      });
    });

    it('putItems puts the error into putting', async () => {
      const commit = jest.fn();
      const state = {};
      const rootState = {};
      const testError = new Error('foo');
      authService.loadIsAuthenticated = () => Promise.reject(testError);
      sut = createParsedMovieStore({ authService });
      // eslint-disable-next-line no-console
      console.error = jest.fn();
      await sut.actions.putItems({ commit, state, rootState });
      expect(commit.mock.calls[0][0]).toEqual('putting');
      expect(commit.mock.calls[0][1]).toBe(true);
      expect(commit.mock.calls[1][0]).toEqual('putting');
      expect(commit.mock.calls[1][1]).toBe(testError);
    });
  });
});

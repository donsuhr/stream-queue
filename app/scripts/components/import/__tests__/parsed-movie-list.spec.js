/* eslint-env jest */

import { shallowMount, createLocalVue } from '@vue/test-utils';
import ParsedMovieList from '../parsed-movie-list';
import createStore from '../../../store';
import authService from '../../../service/__mocks__/auth-service';

const localVue = createLocalVue();
let store;

const items = [
  {
    netflixId: '80116741',
    title: 'My Life as a Zucchini',
  },
  {
    netflixId: '2',
    title: 'Movie 2',
  },
];

describe('parsed-movie-list.vue', () => {
  beforeEach(() => {
    store = createStore({ authService });
  });

  test('renders', () => {
    store.dispatch('parsedMovies/setItems', items);
    const sut = shallowMount(ParsedMovieList, {
      store,
      localVue,
    });
    expect(sut.findAll('li').length).toBe(2);
    expect(sut.find('li').text()).toBe(items[0].title);
  });
});

function createParsedMovieStore({ router, authService, queueService }) {
  const parsedMovieStore = {
    namespaced: true,
    state: {
      items: [],
      putting: false,
    },
    mutations: {
      setItems(state, items) {
        state.items = items;
      },
      putting(state, data) {
        state.putting = data;
      },
    },
    actions: {
      putItems({ commit, state, rootState }) {
        commit('putting', true);
        return authService
          .loadIsAuthenticated()
          .then(() => queueService.patchQueue(state.items))
          .then(() => {
            commit('putting', false);
            router.push({
              name: 'queue',
              params: { filter: 'notset' },
            });
          })
          .catch((err) => {
            // eslint-disable-next-line no-console
            console.error('put error', err);
            commit('putting', err);
          });
      },
      setItems({ commit, state }, items) {
        commit('setItems', items);
      },
    },
    getters: {
      putting: (state) => state.putting,
    },
  };

  return parsedMovieStore;
}

export { createParsedMovieStore };

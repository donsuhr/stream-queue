function getDomDataAttributes(el) {
  return Object.keys(el.attributes)
    .map((x) => ({
      name: el.attributes[x].name,
      value: el.attributes[x].value,
    }))
    .filter((x) => x && x.name && x.name.indexOf('data') !== -1)
    .reduce((accum, current) => {
      accum[current.name.replace('data-', '')] = current.value;
      return accum;
    }, {});
}

export { getDomDataAttributes };

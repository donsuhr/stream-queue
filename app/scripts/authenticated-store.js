import { LS_KEY_EVER_AUTH } from './service/auth-service-const';

function createAuthenticatedStore({ authService }) {
  return {
    namespaced: true,
    state: {
      isAuthenticated: null,
      loading: false,
    },
    mutations: {
      setAuthenticated(state, authenticated) {
        state.isAuthenticated = authenticated;
      },
      setLoading(state, loading) {
        state.loading = loading;
      },
    },
    actions: {
      checkAuth({ commit, state, rootState }) {
        commit('setLoading', true);
        const hasEverLoggedIn = localStorage.getItem(LS_KEY_EVER_AUTH);
        if (hasEverLoggedIn === 'true') {
          authService.loadIsAuthenticated().then((result) => {
            commit('setAuthenticated', result);
            commit('setLoading', false);
          });
        } else {
          commit('setAuthenticated', false);
          commit('setLoading', false);
        }
      },
      setAuth({ commit, state, rootState }, isAuth) {
        commit('setAuthenticated', isAuth);
      },
    },
    getters: {
      loading: (state) => state.loading,
      isAuthenticated: (state) => state.isAuthenticated,
    },
  };
}

export { createAuthenticatedStore };
